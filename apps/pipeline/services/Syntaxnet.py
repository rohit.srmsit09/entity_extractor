import os, json
import xmltodict
import requests
import csv
from sklearn.externals import joblib
import requests
retval = os.getcwd()
def process(content):
  output = []
  abort_if_todo_doesnt_exist(content)
  os.system('cd ' + os.environ['syntaxnet_path'] + ' &&  echo "'+ content +'" | syntaxnet/demo.sh && cp '+os.environ['pos_tagger_path']+' '+os.environ['cp_pos_tagger_path'])
  post = joblib.load(retval + '/pos_tagger.pkl')
  pos=[(tagger['word'],tagger['tag']) for tagger in post]
  P_ENTITY = filter(lambda pos: pos['category'] == 'NOUN', post)
    # print 'post ===>', post
  for i in post:
    del i['d']
  output.append({'sentence': content, 'output':P_ENTITY})
  return {'data': output}


def abort_if_todo_doesnt_exist(content):
  if content == "":
    abort(404, message="content {} can't be empty".format(content))


