import json
from  more_itertools import unique_everseen
from watson_developer_cloud import AlchemyLanguageV1
alchemy_language = AlchemyLanguageV1(api_key='6c8cefbf3b82509c7c2f256ddb3657a12a054982')
def process(text_arr):
	entities = []
	for text in text_arr:
		alchemy_res = json.dumps(alchemy_language.entities(text=text), indent=2)
		# print 'alchemy_res -->',alchemy_res
		alchemy_res = json.loads(alchemy_res)
		# print alchemy_res['entities']
		# return {'res': alchemy_res}
		if(len(alchemy_res['entities'])):
			for ent in alchemy_res['entities']:
				entities.append(ent['text'])

	return {'entities':list(unique_everseen(entities))}			
# alchemy('TURKEY INCOMING AK PARTY LEADER SAYS  AK PARTY WILL CONTINUE TO WORK IN HARMONY, CHANGE OF LEADERSHIP JUST A TOOL TO OPEN WAY FOR MORE SERVICES')