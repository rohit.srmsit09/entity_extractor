from datetime import datetime
from elasticsearch import Elasticsearch
import requests
import json, os
baseurl = os.environ.get('es_url','http://104.196.129.89:9200')
#print os.environ.get('es_db','104.196.154.5')
es = Elasticsearch([{'host': os.environ.get('es_db','104.196.129.89'), 'port': 9200}], timeout=12, max_retries=3, retry_on_timeout=True)


def add(index_data, type_data, doc_id, doc_body):
	try:
		if doc_id is not None:
			return es.index(index=index_data, doc_type=type_data, id=doc_id, body=doc_body )
			
		return es.index(index=index_data, doc_type=type_data, body=doc_body )
	except Exception as e:
		return e
def getById(index_data, type_data, doc_id ):
	try:
		return es.get(index=index_data, doc_type=type_data, id=doc_id)
	except Exception as e:
		return e
def search(index_data , type_data, body_query):
	try:
		return es.search(index=index_data, doc_type=type_data, body=body_query )
	except Exception as e:
		return e
def update(index_data, type_data, doc_id, doc_body):
	try:
		#index_data, type_data, doc_id, doc_body
		return es.update(index=index_data,doc_type=type_data,id=doc_id, body=doc_body)
	except Exception as e:
		return e

def addDBbyRest(dbname):
	try:
		url = baseurl+"/"+dbname
		headers = {'content-type': "application/json"}
		response = requests.request("PUT", url, headers=headers)
		return { "message" : response.text , "status" : 200 } 
	except Exception as e:
		return { "message" : str(e) , "status" : 400 }

def analyzerByRest(dbname, typename):
	try:
		url = baseurl+"/"+dbname+"/"+typename+"/_mapping"
		print url
		payload = {typename:{"properties":{"entity":[],"_key":{"type":"string"},"type":{"type":"string"},"source":[],"definition":[],"context":[],"question":{"type":"string"},"suggest":{"type":"completion","analyzer":"simple","search_analyzer":"simple","payloads":True}}}}
		print payload
		headers = {'content-type': "application/json"}

		response = requests.request("PUT", url, data=payload, headers=headers)

		return { "message" : response.text , "status" : 200 } 
	except Exception as e:
		return { "message" : str(e) , "status" : 400 }

def updateSuggestByRest(dbname, typename, doc_id, doc_body):
	try:
		url = baseurl+""+dbname+"/data"+"/"+doc_id
		# print url
		headers = {
    		'content-type': "application/json"
    	}

		response = requests.request("POST", url, data=doc_body, headers=headers)
		#print(response.text)
		return { "message" : response.text , "status" : 200 }
	except Exception as e:
		return { "message" : str(e), "status" : 400 }

def searchAnswer(answerQueue, entity):
	url = "http://localhost:9200/entity/data/_search"

	payload={"query" : {  "match" : {   "question" : answerQueue  }  } , "highlight" :{  "fields" : {"display" : { "fragment_size" : 500, "highlight_query":{ "match" : { "display" : entity } } } } }  }
	#payload = "{\"query\": {\"match\": {\"question\": \"5454758 5495546 5507638 5435576 5440851 5514631 4837570 5511949 5553606 4896665 5463581 5515426 4797340 5486841 4746091 4684804 4801425 4909439 4924583 5395253 \"}}, \"highlight\": {\"fields\": {\"display\": {\"fragment_size\": 500, \"highlight_query\": {\"match\": {\"display\": \"america \"}}}}}}"
	headers = {
	    'content-type': "application/json",
	    'cache-control': "no-cache",
	    }

	response = requests.request("POST", url, data=json.dumps(payload), headers=headers)
	if response.status_code == 200:
		return json.loads(response.text)
	return { "hits" : { "hits" : [] } }


def deleteELasticDb( dbname ):
	url = "localhost:9200/"+dbname
	resp = request.request('DELETE',url)
	return str(resp.text)

'''
def suggestByRest(dbname, typename, text):
	try:
		url = baseurl+"/"+dbname+"/_suggest"
		

		payload = "{\n    \"suggest\" : {\n        \"text\" : \""+text+"\",\n        \"completion\" : {\n            \"field\" : \"suggest\"\n        }\n    }\n}"
		headers = {
		    'content-type': "application/json"
		    }

		response = requests.request("POST", url, data=payload, headers=headers)

		#print(response.text)
		if response.status_code == 200:
			suggestData = json.loads(response.text)['suggest']
			#print suggestData
			return { "data" : suggestData , "status" : 200 } 
		else:
			return { "message" : response.text, "status" : 200 }
	except Exception as e:
		return { "message" : str(e) , "status" : 400 }

def addSuggestByRest(dbname, typename, doc_id, name):
	try:
		url = baseurl+"/"+dbname+"/"+typename+"/"+doc_id#+music/song/3"

		querystring = {"refresh":"true"}

		payload = { "entity" : name['entity'],
					"_key" : doc_id, 
					"suggest" : { 
						"input": name['entity'], 
						"output": name['entity'],
						"payload" : name
					}
				}
		headers = {'content-type': "application/json"}
		#print payload
		response = requests.request("PUT", url, data=payload, headers=headers, params=querystring)
		#return {"message" : "22"}
		#print(response.text)
		return {"message" : response.text , "status" : 200 } 
	except Exception as e:
		return { "message" : str(e) , "status" : 400 }'''
		
#def getAll
#{u'_id': u'42', u'_index': u'my-index', u'_type': u'test-type', u'_version': 1, u'ok': True}

# but not deserialized
'''es.get(index="my-index", doc_type="test-type", id=42)['_source']
{u'any': u'data', u'timestamp': u'2013-05-12T19:45:31.804229'}'''