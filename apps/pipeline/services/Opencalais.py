# from standfordcorenlp import jsonrpc
from  more_itertools import unique_everseen
from simplejson import loads
import requests
import json
url = "https://api.thomsonreuters.com/permid/calais"
headers = { 'outputFormat': "application/json", 'x-ag-access-token':'bVhFc6RBgyAd5ME9lkzB1dEbv0DPrycG' }
def process(text_arr):
	entities = []
	for text in text_arr:
		payload = text
		response = requests.request("POST", url, data=json.dumps(payload), headers=headers)
		res =  response.json()
		res = json.dumps(res)
		res_load = json.loads(res)
		calis_all_keys = res_load.keys()

		entites_keys =  filter(lambda key: 'http://d.opencalais.com/genericHasher-1' in key, calis_all_keys)

		if len(entites_keys):
			for key in entites_keys:
				# print res_load[key]['name']
				entities.append(res_load[key]['name'])
	return {'entities': list(unique_everseen(entities))}
# print opencalis()