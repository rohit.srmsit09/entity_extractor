import ElasticModule, difflib,datetime,arangoModule,WebhoseRestAPI
from nltk.corpus import stopwords
from multiprocessing import Process, Queue, Pool
import inflect,json,threading
inflect = inflect.engine()

stop = stopwords.words('english')
collection_entity , elasticIndex , elasticType = 'entities', 'entity', 'data'

def entityMatchBackground( query , que ,fields=True , startFrom= 0, size=40  , searchIn="entity product organization fibo" , matchingType ="match_phrase_prefix" ):
  query = { "from" : startFrom , "size" : size , "query" : { "bool" : {  "must" : [{  matchingType : { "display": query } },{ "match" : { "type" : searchIn  }   }] } } }
  if fields:
    query["fields"] = ["display","type","attributes.isin"] 
  data =[]
  #print query , "PARALLEL QUERY"
  try:
    data = ElasticModule.search(elasticIndex, elasticType, query )['hits']['hits']
  except Exception as e:
    data = []
  que.put(data)

def webhoseBackground( query, que ):
  que.put( WebhoseRestAPI.searchAgain(query) )

def suggestion(textQuery, uuid, transnum ):
  try:
    with open("static/Logs", "a") as f:
      f.write(textQuery+ " : "+datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") )  
    query = { "query" : { "bool" : {  "must" : [{  "match_phrase_prefix" : { "display": textQuery } },{ "match" : { "type" : "entity organization product fibo"  }   }] } }, "fields" : "display" }
    #return jsonify(ElasticModule.search("scrap", "data", query )['hits']['hits'])
    data = ElasticModule.search(elasticIndex, elasticType, query )['hits']['hits']
    #print data
    resp = {   }
    l = len(data)
    if l>0:
     
      content = { }
      for x in range(0,l):
        content[ x ] = { "name" : data[x]['fields']['display'][0] , "_key" : data[x]['_id'] , "confidence" : difflib.SequenceMatcher(None, data[x]['fields']['display'][0].lower(), textQuery.lower() ).ratio()  }

      resp['content'] = content
      return resp

    resp['header']['responseCode'] = "ERR"
    resp['header']['responseMessage'] = "No Record Found"
    return resp

  except Exception as e:
    return {"message" : str(e),"status" : 400 }  

def getexactMatchV2( textQuery, uuid, transnum  ):
  try:
    textQuery = textQuery.replace(' ','_').lower()
    if textQuery[-1:] == "s":
      back = textQuery[0:-1]
    else:
      back = textQuery+"s"
    #print back, textQuery

    #query = { "query" : { "term"  : {"_key" :  textQuery  }  } }
    data = ElasticModule.getById(elasticIndex,elasticType,textQuery)#ElasticModule.search(elasticIndex, elasticType, query )['hits']['hits']
    resp = {  }
    if type(data) == type({}) or data == type([]):
        #print data
        content = {}
        content["0"] = {  "name"  : data['_source']['display'][0] , "_key" : data['_id'] , "definition" : [ d['definition'] for d in data['_source']['attributes'] ]  } 
        resp['content'] = { "entities" :  content}
        #return resp
        if textQuery == data['_id']:
          return resp

    data = ElasticModule.getById(elasticIndex,elasticType,back)
    typeCheck = type(data)
    if typeCheck == type({}) or typeCheck == type([]):

        content = {}
        content["0"] = {  "name"  : data['_source']['display'][0] , "_key" : data['_id'] , "definition" : [ d['definition'] for d in data['_source']['attributes'] ]  } 
        resp['content'] = { "entities" :  content}
        return resp

    return { "content" : None, "message" : "no record" }
    

  except Exception as e:
    return { "content" : None, "message" : str(e) }

def searchRelatedToEntity( entity ,uuid, transnum ):

  s=""
  for i in entity.split():
    if i not in stop:
      s+=i+" "
  """
  q = Queue()
  p = Process(target = webhoseBackground, args= ( s, q) ) 
  p.start() 
  """
  
  newsQueue   = Queue() #query , que ,fields=True , startFrom= 0, size=40  , searchIn="entity product organization fibo" , matchingType ="match_phrase_prefix" 

  print s, "query being fired"

  query = { "from" : 0 , "size" : 20 , "query" : { "bool" : {  "must" : [{  "match" : { "display": s } },{ "match" : { "type" : "entity product organization fibo"  }   }] } }}
  print "debug query entity 1"
  data = ElasticModule.search(elasticIndex, elasticType, query )['hits']['hits'] 
  print "debug query entity 11"
  resp = { } 
  l = len(data)
  content = { "entities" : []  , "questions" : [] , "webhose" : [] , "feedly" : [] , "news" : [] }
  strCheck = type('')
  if l>0:
      for x in range(0,l):

      #print "type 2 debug"
        try:
          typeOf=data[x]['_source']['type']
          if data[x]['_source']['type'] in ["organization" , "product"]:

              if data[x]['_source']['type'][0] =='product':
                typeOf = "etf"
              content['entities'].append( { "name" : data[x]['_source']['display'][0] , "_key" : data[x]['_id'] , "definition" : [ d['definition'] for d in data[x]['_source']['attributes'] if d['definition']!="" ]  , "type" : typeOf , "isin" : [ is_in.get('isin','') for is_in in data[x]['_source']['attributes'] ][0]  } )
          else:
              content['entities'].append( { "name" : data[x]['_source']['display'][0] , "_key" : data[x]['_id'] , "definition" : [ d['definition'] for d in data[x]['_source']['attributes'] if d['definition']!="" ]  , "type" : typeOf })
        except Exception as e:
          pass
  
  questionQuery = { "from" : 0 , "size" : 20 , "query" : { "bool" : {  "must" : [{  "match" : { "display": entity } },{ "match" : { "type" : "question"  }   }] } }}
  print "debug query entity 2"
  data = ElasticModule.search(elasticIndex, elasticType, questionQuery)['hits']['hits']
  print "debug query after entity 22"
  l = len(data)
  #strCheck = type('')
  if l>0:
      answerQueue = ""
      for x in range(0,l):  
        if data[x]['_source']['type'] == "question":
          #print type(data[x]['_source']['display']) , "saved type "
          if len( data[x]['_source']['display'][0] ) == 1 :
            ques = data[x]['_source']['display']
          else:
            ques = data[x]['_source']['display'][0]
          confidenceScore = difflib.SequenceMatcher(None, ques.lower(), entity.lower() ).ratio()
          qaData = { "question" : ques , "confidence" : confidenceScore , "hint" : data[x]['_source'].get('hint','') , "id" : data[x]['_id'] }
          #content['questions'].append({ "question" : ques , "confidence" : confidenceScore , "hint" : data[x]['_source'].get('hint','') })

          #ans = arangoModule.get('answers' , { "question" : data[x]['_id'] } )
          #if ans is not None:
          #  qaData['answer'] = ans[0]['answer']
          #  qaData['sourceAnswer'] = ans[0].get('source','yahoo answers')
            
          #  content['answers'].append({ "answer" : ans[0]['answer']  , "confidence" : confidenceScore , "source" : ans[0].get('source','yahoo answers')})
          answerQueue+=data[x]['_id']+" "
          content['questions'].append(qaData)
    
      answersFetch = {"query" : {  "match" : {   "question" : answerQueue  }  } , "highlight" :{  "fields" : {"display" : { "fragment_size" : 500, "highlight_query":{ "match" : { "display" : s } } } } }  }
      print answersFetch
      answerData   = ElasticModule.search(elasticIndex, elasticType, answersFetch)['hits']['hits']
      for quesObject in content['questions']:
        for ansObject in answerData:
          if quesObject['id'] == ansObject['_source']['question']:
            highlightText = ansObject.get('highlight',None)
            if highlightText is not None:
              quesObject['answer'] = highlightText['display'][0]       
            else:
              quesObject['answer'] = ansObject['_source']['display']
            quesObject['source'] = ansObject['_source']['source']
            break
          

          #if quesObject['id'] == ansObject['_source']['question']:
          #  quesObject['answer'] = ansObject
      #    print quesObject['id'], ansObject


  """    
  p.join()
  #posts = q.get()['posts']
  #print posts,"err here"
  for x in posts:
    print "webhose post"
    content['webhose'].append({ "highlighText" :  x['highlightText'] , "title" : x['title'] , "url" : x['url'] , "published" : x['published']    })
  """
  newsProcess = Process(target = entityMatchBackground, args= ( s, newsQueue, False, 0, 20, "news", "match") )
  newsProcess.start() 
  newsProcess.join()
  newsPosts = newsQueue.get()
  if newsPosts!=[]:
    #print newsPosts, "record from es"
    for newsData in newsPosts:
      src = newsData['_source']['attributes'][0]
      
      if newsData['_source'].get('context-type', '' ) == "rabbitmq":
          content['news'].append({  "title" : src.get('title','') , "definition" : src['definition'] , "source" : src['source'] , "published" : src.get('published','')  })
      else:
          content['feedly'].append( {  "title" : src.get('title','') , "definition" : src['definition'] , "source" : src['source'] , "published" : src.get('published','')  } )
     # content['feedly'].append( {  "title" : src.get('title','') , "definition" : src['definition'] , "source" : src['source'] , "published" : src.get('published','')  } )


  content['questions'] = sorted(content['questions'] , key = lambda k:  k['confidence'] , reverse = True )

  resp['content'] = content
  return resp

def entitySuggester( textQuery, uuid, transnum  ):
  try:
    entityWords = textQuery.split(' ')
    filteredWords = [ ]
    l = len(entityWords)
    if l>0:
      for x in entityWords:
        if x not in stop:
          plural = inflect.singular_noun(x)
          if plural == False:
            filteredWords.append(x)
          else:
            filteredWords.append(x)
            filteredWords.append(plural)
      
      for x in range(0, l):
          filteredWords.append( '_'.join( entityWords[x:] ) )
          filteredWords.append( '_'.join(entityWords[:x]  ) )

    filteredWords = list(set(filteredWords))
    #print filteredWords, "<---"
    query = { "query" : { "bool" : {  "must" : [{  "match_phrase_prefix" : { "display": textQuery } },{ "match" : { "type" : "entity product organization fibo country"  }   }] } }, "fields" : ["display","type", "attributes.isin"] }

    data = ElasticModule.search(elasticIndex, elasticType, query )['hits']['hits']
    #print data
    resp = {}
    l = len(data)
    index = 0
    content = []
    filterName = []
    if l>0:
      
      for x in range(0,l):
        index = x
        #content[ index ] = { "name" : data[x]['fields']['display'][0] , "_key" : data[x]['_id'] , "confidence" : difflib.SequenceMatcher(None, data[x]['fields']['display'][0].lower(), textQuery.lower() ).ratio() , "type" : data[x]['fields']['type'][0] }
        if data[x]['fields']['type'][0] in ["organization" , "product"]:
            typeOf ="organization"
            if data[x]['fields']['type'][0] =='product':
              typeOf = "etf"
            content.append( { "name" : data[x]['fields']['display'][0] , "_key" : data[x]['_id'] , "confidence" : difflib.SequenceMatcher(None, data[x]['fields']['display'][0].lower(), textQuery.lower() ).ratio() , "type" : typeOf , "isin" : data[x]['fields'].get('attributes.isin',[None])[0] })
        else:
            content.append( { "name" : data[x]['fields']['display'][0] , "_key" : data[x]['_id'] , "confidence" : difflib.SequenceMatcher(None, data[x]['fields']['display'][0].lower(), textQuery.lower() ).ratio() , "type" : data[x]['fields']['type'][0] })
        filterName.append( data[x]['fields']['display'][0] )

    resultsFromQueue = []
    for x in filteredWords:
      if x not in filterName:
        q = Queue()
        p = Process(target = entityMatchBackground, args= ( x, q) ) 
        p.start()
        p.join()
        resultsFromQueue.append({ "value" : x , "resultset" :  q.get() } )
    
    for x in resultsFromQueue:
        typeX = type( x['resultset'] )
        if typeX == type([]):
          for entity_data in x['resultset']:
            index+=1
            if entity_data['fields']['type'][0] in ["organization", "product"]:
              typeOf = "organization"
              if entity_data['fields']['type'][0] == "product":
                typeOf = "etf"

              content.append( {  "name"  : entity_data['fields']['display'][0] , "_key" : entity_data['_id'] , "confidence" : difflib.SequenceMatcher(None, entity_data['_id'].lower(), x['value'].lower() ).ratio() , "type" : typeOf, "isin" : entity_data['fields'].get('attributes.isin',[None])[0] } )
            else:
              content.append( {  "name"  : entity_data['fields']['display'][0] , "_key" : entity_data['_id'] , "confidence" : difflib.SequenceMatcher(None, entity_data['_id'].lower(), x['value'].lower() ).ratio() , "type" : entity_data['fields']['type'][0]  } )

    content = sorted(content, key = lambda k:  k['confidence'] , reverse = True )
    resp['content'] = { "entities" : content }
    if len(content)>0:
      return resp

    return { "content" : None , "message" : "no record"}

  except Exception as e:
    return {"message" : str(e), "content" : None}  

def getByHint(hint, uuid, transnum ):
  resp = {   } 
  
  query = {"query" : { "bool" : {  "must" : [{  "match_phrase_prefix" : { "hint": hint } },{ "match" : { "type" : "question"  }   }] } } }
  data = ElasticModule.search(elasticIndex, elasticType, query )['hits']['hits'] 
  if len(data)>0:
      content = { "questions" : []  }
      for x in data:
        print x
        if len( x['_source']['display'][0] ) == 1 :
          ques = x['_source']['display']
        else:
          ques = x['_source']['display'][0]
        
        #content['questions'].append({ "question" : ques, "hint" : data[0]['_source'].get('hint','') })
        qaData = { "question" : ques, "hint" : x['_source'].get('hint','') }
        
        ansObject = ElasticModule.searchAnswer( x['_id'], hint)['hits']['hits']
        if len(ansObject)>0:
          highlightText = ansObject[0].get('highlight',None)
          if highlightText is not None:
            qaData['answer'] = highlightText['display'][0]       
          else:
            qaData['answer'] = ansObject[0]['_source']['display']
          qaData['source'] = ansObject[0]['_source'].get('source','Forward Lane')

        '''
        ans = arangoModule.get('answers' , { "question" : x['_id'] } )
        if ans is not None:
          #content['answers'].append({ "answer" : ans[0]['answer'] , "source" : ans[0].get('source','yahoo answers')})
          qaData['answer'] = ans[0]['answer']
          qaData['source'] = ans[0].get('source','ForwardLane')
        '''


        content['questions'].append(qaData)
      resp['content'] = content
      return resp
  else:
      return { "content" : None , "message" : "no record" }


def elasticQuery( query ,fields=True , startFrom=0, size=40  , searchIn="entity product organization fibo" , matchingType ="match_phrase_prefix" ):
  if searchIn == "product etf organization":
    query = { "from" : startFrom , "size" : size , "query" : { "bool" : {  "must" : [{ "bool": {"should": [{matchingType : { "display": query }}, {matchingType : { "attributes.ticker": query }}] }},{ "match" : { "type" : searchIn  }   }] } } }
  else:
    query = { "from" : startFrom , "size" : size , "query" : { "bool" : {  "must" : [{  matchingType : { "display": query } },{ "match" : { "type" : searchIn  }   }] } } }
  if fields:
    query["fields"] = ["display","type","attributes.isin"] 
  data =[]
  try:
    data = ElasticModule.search(elasticIndex, elasticType, query )['hits']['hits']
  except Exception as e:
    data = []
  return data      

def queryEntity( entity ,uuid, transnum , startFrom = 0, endSize= 20, filterData=False ):
  try:
    s=""
    for i in entity.split():
      if i not in stop:
        s+=i+" "
    #disp =entity
    #if filterData:
    #  disp = { "query" : entity , "operator" : "and"  }
    s=s.strip(" ")
    print s , " query entity"
    questionQuery = { "from" : 0 , "size" : 10 , "query" : { "bool" : {  "must" : [{  "match" : { "display": entity } },{ "match" : { "type" : "question"  }   }] } }}
    #print questionQuery

    #finalData           = []
    #parallelProcs       = []
    #print "parallelprocs 0"
    parallelProcs =[ ElasticModule.search(elasticIndex, elasticType, questionQuery) ,
    elasticQuery( s, False, startFrom, endSize, "news", "match"  )  ,
    elasticQuery( s, False, startFrom, endSize, "entity product organization fibo country", "match" ) ]

    print "parallelprocs 0 debug ->",parallelProcs[0]
    data = parallelProcs[0]['hits']['hits'] 

    #print "parallelprocs 2"
    l = len(data)
    content = { "entities" : []  , "questions" : [] , "webhose" : [] , "feedly" : [] , "news" : [] }
    #answerQueue=""
    if l>0:
        #answerQueue = ""
        #print "parallelprocs 3"
        for x in range(0,l):  
          try:
            if data[x]['_source']['type'] == "question":
              #print type(data[x]['_source']['display']) , "saved type "
              if len( data[x]['_source']['display'][0] ) == 1 :
                ques = data[x]['_source']['display']
              else:
                ques = data[x]['_source']['display'][0]
              confidenceScore = difflib.SequenceMatcher(None, ques.lower(), entity.lower() ).ratio()
              qaData = { "question" : ques , "confidence" : confidenceScore , "hint" : data[x]['_source'].get('hint','') , "id" : data[x]['_id'] }
     
              #answerQueue+=data[x]['_id']+" "
              ansObject = ElasticModule.searchAnswer(data[x]['_id'], s)['hits']['hits']
              if len(ansObject)>0:
                highlightText = ansObject[0].get('highlight',None)
                if highlightText is not None:
                  qaData['answer'] = highlightText['display'][0]       
                else:
                  qaData['answer'] = ansObject[0]['_source']['display']
                qaData['source'] = ansObject[0]['_source'].get('source','')

              content['questions'].append(qaData)

          except Exception as e:
            print e, data[x], "question issue"
            pass
        content['questions'] = sorted(content['questions'] , key = lambda k:  k['confidence'] , reverse = True )

    newsPosts = parallelProcs[1]
    if newsPosts!=[]:
      try:
        for newsData in newsPosts:
          #print newsData, "#news data"
          src = newsData['_source'].get('attributes', None)
          if src is not None:
            src = src[0]
            if newsData['_source'].get('context-type','') == "rabbitmq":
              titles = src.get('titles',None)
              if titles is not None:
                titles = titles[-1:]
              content['news'].append({ "id" : newsData['_id'], "title" : titles[0] , "newsText" : src['definition'] , "source" : src['source'] , "published" : src.get('published_ts','') , "confidence" : difflib.SequenceMatcher(None, s.lower(), titles[0].lower()  ).ratio() })
            else:
              titles = src.get('titles',[''])
              content['feedly'].append( { "id" : newsData['_id'], "title" : titles[0] , "newsText" : src['definition'] , "source" : src['source'] , "published" : src.get('published_ts','') , "confidence" : difflib.SequenceMatcher(None, s.lower(), titles[0].lower()  ).ratio()   } )
        content['news'] = sorted(content['news'] , key = lambda k:  k['confidence'] , reverse = True )
        content['feedly'] = sorted(content['feedly'] , key = lambda k:  k['confidence'] , reverse = True )
      except Exception  as e:
        print e , " issue here on news"
        pass   

    #print "parallel procs 6"
    data = parallelProcs[2]
    l = len(data)
    strCheck = type('')
    if l>0:
        for x in range(0,l):
            try:
              typeOf=data[x]['_source']['type']
              confidence = difflib.SequenceMatcher(None, s.lower(), data[x]['_source']['display'][0].lower()  ).ratio()
              if data[x]['_source']['type'] in ["organization" , "product"]:

                  #if data[x]['_source']['type'] =='product':
                  #  typeOf = "etf"
                  content['entities'].append( { "name" : data[x]['_source']['display'][0] , "_key" : data[x]['_id'] , "definition" : [ d['definition'] for d in data[x]['_source']['attributes'] if d['definition']!="" ]  , "type" : typeOf ,"isin":[ is_in.get('isin','') for is_in in data[x]['_source']['attributes'] ][0] , "confidence" : confidence } )
              elif typeOf == "fibo":
                  content['entities'].append( { "name" : data[x]['_source']['display'][0] , "_key" : data[x]['_id'] ,"definition" : [ d['definition'] for d in data[x]['_source']['attributes'] if d.get('definition', None) != None ],"type" : typeOf , "confidence" : confidence })
              elif typeOf !="question":
                  content['entities'].append( { "name" : data[x]['_source']['display'][0] , "_key" : data[x]['_id'] , "definition" : [ d['definition'] for d in data[x]['_source']['attributes'] if d.get('definition', None)!= None ]  , "type" : typeOf , "confidence" : confidence })
            except Exception as e:
              print e, data[x],"## issue"
              pass
    return { "content" : content }
  except Exception as e:
    return { "content" : None , "message" : str(e) }

def relevantQA( entity, startFrom =0, size=40   ):
  try:
    s=""
    for i in entity.split():
      if i not in stop:
        s+=i+" "
    questionQuery = { "from" : 0 , "size" : 10 , "query" : { "bool" : {  "must" : [{  "match" : { "display": entity } },{ "match" : { "type" : "question"  }   }] } }}
    #print questionQuery
    #finalData           = []
    data = ElasticModule.search(elasticIndex, elasticType, questionQuery)['hits']['hits']
    l = len(data)

    content = {  "questions" : []  }
    #answerQueue=""
    if l>0:
        #answerQueue = ""
        for x in range(0,l):  

            if len( data[x]['_source']['display'][0] ) == 1 :
              ques = data[x]['_source']['display']
            else:
              ques = data[x]['_source']['display'][0]
            confidenceScore = difflib.SequenceMatcher(None, ques.lower(), entity.lower() ).ratio()
            qaData = { "question" : ques , "confidence" : confidenceScore , "hint" : data[x]['_source'].get('hint','') , "id" : data[x]['_id'] }

            ansObject = ElasticModule.searchAnswer(data[x]['_id'], s)['hits']['hits']
            if len(ansObject)>0:
              highlightText = ansObject[0].get('highlight',None)
              if highlightText is not None:
                qaData['answer'] = highlightText['display'][0]       
              else:
                qaData['answer'] = ansObject[0]['_source']['display']
              qaData['source'] = ansObject[0]['_source']['source']

            content['questions'].append(qaData)

    return {  "content" : content }
  except Exception as e:
    print e
    return { "content" : None }

def getEntityNews( s, q, startFrom = 0, endSize= 20 ):

    newsPosts = elasticQuery( s, False, startFrom, endSize, "news", "match"  )
    content = { "news" : [] , "feedly" : [] } 
    if newsPosts!=[]:
      try:
        for newsData in newsPosts:
          #print newsData, "#news data"
          src = newsData['_source'].get('attributes', None)
          if src is not None:
            src = src[0]
            if newsData['_source'].get('context-type','') == "rabbitmq":
              titles = src.get('titles',None)
              if titles is not None:
                titles = titles[-1:]
              content['news'].append({ "id" : newsData['_id'], "title" : titles[0] , "newsText" : src['definition'] , "source" : src['source'] , "published" : src.get('published_ts','') , "confidence" : difflib.SequenceMatcher(None, s.lower(), titles[0].lower()  ).ratio() })
            else:
              titles = src.get('titles',[''])
              content['feedly'].append( { "id" : newsData['_id'], "title" : titles[0] , "newsText" : src['definition'] , "source" : src['source'] , "published" : src.get('published_ts','') , "confidence" : difflib.SequenceMatcher(None, s.lower(), titles[0].lower()  ).ratio()   } )
        content['news'] = sorted(content['news'] , key = lambda k:  k['confidence'] , reverse = True )
        content['feedly'] = sorted(content['feedly'] , key = lambda k:  k['confidence'] , reverse = True )
      except Exception  as e:
        print e , " issue here on news"
        pass   

    q.put(content)
    #return content

def getEntityNewsV2( s, q, existingFeedbacks, startFrom = 0, endSize= 20 ):

    newsPosts = elasticQuery( s, False, startFrom, endSize, "news", "match"  )
    content = { "news" : [] , "feedly" : [] } 
    if newsPosts!=[]:
      try:
        for newsData in newsPosts:
          #print newsData, "#news data"
          src = newsData['_source'].get('attributes', None)
          if src is not None:
            src = src[0]
            if newsData['_source'].get('context-type','') == "rabbitmq":
              titles = src.get('titles',None)
              if titles is not None:
                titles = titles[-1:]
              content['news'].append({ "id" : newsData['_id'], "title" : titles[0] , "newsText" : src['definition'] , "source" : src['source'] , "published" : src.get('published_ts','') , "confidence" : difflib.SequenceMatcher(None, s.lower(), titles[0].lower()  ).ratio() })
            else:
              titles = src.get('titles',[''])
              content['feedly'].append( { "id" : newsData['_id'], "title" : titles[0] , "newsText" : src['definition'] , "source" : src['source'] , "published" : src.get('published_ts','') , "confidence" : difflib.SequenceMatcher(None, s.lower(), titles[0].lower()  ).ratio()   } )

        if existingFeedbacks is not None:
          for fb in existingFeedbacks:
            for idx in range(0, len(content['news'])):
              if fb['doc_id'] == content['news'][idx]['id']:
                content['news'][idx]['confidence'] = fb['confidence']

        if existingFeedbacks is not None:
          for fb in existingFeedbacks:
            for idx in range(0, len(content['feedly'])):
              if fb['doc_id'] == content['feedly'][idx]['id']:
                content['feedly'][idx]['confidence'] = fb['confidence'] 
              
        content['news'] = sorted(content['news'] , key = lambda k:  k['confidence'] , reverse = True )
        content['feedly'] = sorted(content['feedly'] , key = lambda k:  k['confidence'] , reverse = True )
      except Exception  as e:
        print e , " issue here on news"
        pass 

    q.put(content)
    #return content
        
def getEntityQuestions( entity , s, q):
  print entity, s
  questionQuery = { "from" : 0 , "size" : 10 , "query" : { "bool" : {  "must" : [{  "match" : { "display": entity } },{ "match" : { "type" : "question"  }   }] } }}
  result = ElasticModule.search(elasticIndex, elasticType, questionQuery)
  data = result['hits']['hits'] 
  l = len(data)
  contentQuestions = [] 
  if l>0:

      for x in range(0,l):  
        try:
          if data[x]['_source']['type'] == "question":
            #print type(data[x]['_source']['display']) , "saved type "
            if len( data[x]['_source']['display'][0] ) == 1 :
              ques = data[x]['_source']['display']
            else:
              ques = data[x]['_source']['display'][0]
            confidenceScore = difflib.SequenceMatcher(None, ques.lower(), entity.lower() ).ratio()
            qaData = { "question" : ques , "confidence" : confidenceScore , "hint" : data[x]['_source'].get('hint','') , "id" : data[x]['_id'] }
   
            #answerQueue+=data[x]['_id']+" "
            ansObject = ElasticModule.searchAnswer(data[x]['_id'], s)['hits']['hits']
            if len(ansObject)>0:
              highlightText = ansObject[0].get('highlight',None)
              if highlightText is not None:
                qaData['answer'] = highlightText['display'][0]       
              else:
                qaData['answer'] = ansObject[0]['_source']['display']
              qaData['source'] = ansObject[0]['_source'].get('source','')

            contentQuestions.append(qaData)

        except Exception as e:
          print e, data[x], "question issue"
          pass
      contentQuestions = sorted(contentQuestions , key = lambda k:  k['confidence'] , reverse = True )

  q.put(contentQuestions)    
  #return contentQuestions    

def getEntityQuestionsV2( entity , s, q, existingFeedbacks):
  print entity, s
  questionQuery = { "from" : 0 , "size" : 10 , "query" : { "bool" : {  "must" : [{  "match" : { "display": entity } },{ "match" : { "type" : "question"  }   }] } }}
  result = ElasticModule.search(elasticIndex, elasticType, questionQuery)
  data = result['hits']['hits'] 
  l = len(data)
  contentQuestions = [] 
  if l>0:

      for x in range(0,l):  
        try:
          if data[x]['_source']['type'] == "question":
            #print type(data[x]['_source']['display']) , "saved type "
            if len( data[x]['_source']['display'][0] ) == 1 :
              ques = data[x]['_source']['display']
            else:
              ques = data[x]['_source']['display'][0]
            confidenceScore = difflib.SequenceMatcher(None, ques.lower(), entity.lower() ).ratio()
            qaData = { "question" : ques , "confidence" : confidenceScore , "hint" : data[x]['_source'].get('hint','') , "id" : data[x]['_id'] }
   
            #answerQueue+=data[x]['_id']+" "
            ansObject = ElasticModule.searchAnswer(data[x]['_id'], s)['hits']['hits']
            if len(ansObject)>0:
              highlightText = ansObject[0].get('highlight',None)
              if highlightText is not None:
                qaData['answer'] = highlightText['display'][0]       
              else:
                qaData['answer'] = ansObject[0]['_source']['display']
              qaData['source'] = ansObject[0]['_source'].get('source','')

            contentQuestions.append(qaData)

        except Exception as e:
          print e, data[x], "question issue"
          pass

      if existingFeedbacks is not None:
        for fb in existingFeedbacks:
          for idx in range(0, len(contentQuestions)):
            if fb['doc_id'] == contentQuestions[idx]['id']:
              contentQuestions[idx]['confidence'] = fb['confidence']    

      contentQuestions = sorted(contentQuestions , key = lambda k:  k['confidence'] , reverse = True )

  q.put(contentQuestions)    
  #return contentQuestions    

def getEntityRelated( s , type_list, q, startFrom=0, endSize=20 ):
  s = s.lower()
  data = elasticQuery( s, False, startFrom, endSize, type_list, "match" )
  l = len(data)
  strCheck = type('')
  contentEntities = []
  if l>0:
      for x in range(0,l):
          try:
            typeOf=data[x]['_source']['type']
            confidence = difflib.SequenceMatcher(None, s.lower(), data[x]['_source']['display'][0].lower()  ).ratio()
            if data[x]['_source']['type'] in ["organization" , "product", "etf"]:
                #if data[x]['_source']['type'] =='product':
                #  typeOf = "etf"
                contentEntities.append( { "name" : data[x]['_source']['display'] , "_key" : data[x]['_id'] , "definition" : [ d['definition'] for d in data[x]['_source']['attributes'] if d.get('definition', None) != None ], "type" : typeOf, "isin":[ is_in.get('isin','') for is_in in data[x]['_source']['attributes'] ], "confidence" : confidence, "ticker":[ is_in.get('ticker','') for is_in in data[x]['_source']['attributes'] ]  } )
            elif typeOf == "fibo":
                contentEntities.append( { "name" : data[x]['_source']['display'] , "_key" : data[x]['_id'] ,"definition" : [ d['definition'] for d in data[x]['_source']['attributes'] if d.get('definition', None) != None ], "type" : typeOf, "confidence" : confidence })
            elif typeOf !="question":
                contentEntities.append( { "name" : data[x]['_source']['display'] , "_key" : data[x]['_id'] , "definition" : [ d['definition'] for d in data[x]['_source']['attributes'] if d.get('definition', None) != None ], "type" : typeOf, "confidence" : confidence })
          except Exception as e:
            print e, data[x],"## issue"
            pass

      contentEntities = sorted(contentEntities , key = lambda k:  k['confidence'] , reverse = True )
  q.put(contentEntities)
  #return contentEntities  
  
def getEntityRelatedV2( s , type_list, q, existingFeedbacks, startFrom=0, endSize=20 ):
  s = s.lower()
  data = elasticQuery( s, False, startFrom, endSize, type_list, "match" )
  l = len(data)
  strCheck = type('')
  contentEntities = []
  if l>0:
      for x in range(0,l):
          try:
            typeOf=data[x]['_source']['type']
            confidence = difflib.SequenceMatcher(None, s.lower(), data[x]['_source']['display'][0].lower()  ).ratio()
            if data[x]['_source']['type'] in ["organization" , "product", "etf"]:
                contentEntities.append( { "name" : data[x]['_source']['display'] , "_key" : data[x]['_id'] , "definition" : [ d['definition'] for d in data[x]['_source']['attributes'] if d.get('definition', None) != None ], "type" : typeOf, "isin":[ is_in.get('isin','') for is_in in data[x]['_source']['attributes'] ], "confidence" : confidence, "ticker":[ is_in.get('ticker','') for is_in in data[x]['_source']['attributes'] ]  } )
            elif typeOf == "fibo":
                contentEntities.append( { "name" : data[x]['_source']['display'] , "_key" : data[x]['_id'] ,"definition" : [ d['definition'] for d in data[x]['_source']['attributes'] if d.get('definition', None) != None ], "type" : typeOf, "confidence" : confidence })
            elif typeOf !="question":
                contentEntities.append( { "name" : data[x]['_source']['display'] , "_key" : data[x]['_id'] , "definition" : [ d['definition'] for d in data[x]['_source']['attributes'] if d.get('definition', None) != None ], "type" : typeOf, "confidence" : confidence })
          except Exception as e:
            print e, data[x],"## issue"
            pass

      if existingFeedbacks is not None:
        for fb in existingFeedbacks:
          for idx in range(0, len(contentEntities)):
            if fb['doc_id'] == contentEntities[idx]['_key']:
              contentEntities[idx]['confidence'] = fb['confidence']

      contentEntities = sorted(contentEntities , key = lambda k:  k['confidence'] , reverse = True )
  q.put(contentEntities)
  #return contentEntities  

def getEntityData( entity ,uuid, transnum , startFrom = 0, endSize= 20, filterData=False  ):
  try:
    s=""
    for i in entity.split():
      if i not in stop:
        s+=i+" "
    s=s.strip(" ")
    print s , " query entity"
    content = { "entities" : []  , "questions" : [] , "webhose" : [] , "feedly" : [] , "news" : [] }

    q1,q2,q3,q4 = Queue(),Queue(),Queue(),Queue()
    parallelprocs = [
    threading.Thread(target=getEntityRelated, args=(s, "entity fibo country", q1 ) ),
    threading.Thread(target=getEntityQuestions, args=(entity, s, q2 ) ),  
    threading.Thread(target=getEntityNews, args=(s, q3 ) ),
    threading.Thread(target=getEntityRelated, args=(s, "product etf organization", q4 ) )  ]

    for x in parallelprocs:
      x.start()

    unsortedEntities1 = q1.get()
    unsortedEntities2 = q4.get()
    content['entities'] = unsortedEntities1 + unsortedEntities2
    content['entities'] = sorted(content['entities'], key = lambda k:  k['confidence'] , reverse = True )
    content['questions'] = q2.get()

    newsFeedly = q3.get()
    for x in parallelprocs:
      x.join()
    content['feedly'] = newsFeedly['feedly']
    content['news'] = newsFeedly['news']
    return { "content" : content }
  except Exception as e:
    return {"content" : { "entities" : []  , "questions" : [] , "webhose" : [] , "feedly" : [] , "news" : [] } }

def getEntityDataV2( entity ,uuid, transnum , existingFeedbacks, startFrom = 0, endSize= 20, filterData=False  ):
  try:
    s=""
    for i in entity.split():
      if i not in stop:
        s+=i+" "
    s=s.strip(" ")
    print s , " query entity"
    content = { "entities" : []  , "questions" : [] , "webhose" : [] , "feedly" : [] , "news" : [] }

    q1,q2,q3,q4 = Queue(),Queue(),Queue(),Queue()
    parallelprocs = [
    threading.Thread(target=getEntityRelatedV2, args=(s, "entity fibo country", q1, existingFeedbacks, 0, 10 ) ),
    threading.Thread(target=getEntityQuestionsV2, args=(entity, s, q2, existingFeedbacks) ),  
    threading.Thread(target=getEntityNewsV2, args=(s, q3, existingFeedbacks) ),
    threading.Thread(target=getEntityRelatedV2, args=(s, "product etf organization", q4, existingFeedbacks, 0, 10) )  ]

    for x in parallelprocs:
      x.start()

    unsortedEntities1 = q1.get()
    unsortedEntities2 = q4.get()
    content['entities'] = unsortedEntities1 + unsortedEntities2
    content['entities'] = sorted(content['entities'], key = lambda k:  k['confidence'] , reverse = True )
    content['questions'] = q2.get()

    newsFeedly = q3.get()
    for x in parallelprocs:
      x.join()
    content['feedly'] = newsFeedly['feedly']
    content['news'] = newsFeedly['news']
    return { "content" : content }
  except Exception as e:
    return {"content" : { "entities" : []  , "questions" : [] , "webhose" : [] , "feedly" : [] , "news" : [] } }

def migrateQuestions():
  questions = arangoModule.get('questions', {})
  print "total questions === ", len(questions)
  esErrorData = []
  count = 1
  for qn in questions:
    payload = {
      "_key" : qn['_key'],
      "type" : "question",
      "hint" : qn.get('hint', []),
      "time_stamp": qn.get('time_stamp', None)
    }

    if type(qn['ques']) == type([]):
      payload['display'] = qn['ques']
    else:
      payload['display'] = [qn['ques']]
    print "payload == ", payload
    es_resp = ElasticModule.add(elasticIndex, elasticType, qn['_key'], payload) 
    if "TransportError" in str(es_resp):
      file_data = {"exception" : str(es_resp), "payload": payload}
      esErrorData.append(file_data)
    print "es_resp == ", es_resp
    print "count == ", count
    count = count + 1
    print "\n"
  if len(esErrorData) > 0:
    with open("./entity_logs/questions_es_index_exception.txt", "w") as f:
      json.dump(esErrorData, f)

  print "questions es index completed"

def migrateAnswers():
  answers = arangoModule.get('answers', {})
  print "total answers === ", len(answers)
  count = 1
  esErrorData = []
  for answer in answers:
    payload = {
      "_key" : answer['_key'],
      "source": answer.get('source', None),
      "type" : "answer",
      "time_stamp": answer.get('time_stamp', None),
      "display": answer.get('answer', None)
    }

    if type(answer['question']) == type([]):
      payload['question'] = answer['question']
    else:
      payload['question'] = [answer['question']]
    print "payload == ", payload
    es_resp = ElasticModule.add(elasticIndex, elasticType, answer['_key'], payload) 
    if "TransportError" in str(es_resp):
      file_data = {"exception" : str(es_resp), "payload": payload}
      esErrorData.append(file_data)
    print "es_resp == ", es_resp
    print "count == ", count
    count = count + 1
    print "\n"
  if len(esErrorData) > 0:
    with open("./entity_logs/answers_es_index_exception.txt", "w") as f:
      json.dump(esErrorData, f)

  print "answers es index completed"

def migrateEntities():
  entities = arangoModule.get('entities', {})
  print "total entities === ", len(entities)
  esErrorData = []
  data = {"entity": [], "fibo": [], "product": [], "organization": [], "etf": [], "country": []}
  count = 1
  for entity in entities:
    typeof = entity['type']
    data[typeof].append(entity['type'])
    payload = {
      "_key" : entity['_key'],
      "type" : entity['type'],
      "attributes": entity.get('attributes', []),
      "created_at": entity.get('created_at', None),
      "display": entity.get('display', [])
    }
    print "payload == ", payload
    es_resp = ElasticModule.add(elasticIndex, elasticType, entity['_key'], payload) 
    if "TransportError" in str(es_resp):
      file_data = {"exception" : str(es_resp), "payload": payload}
      esErrorData.append(file_data)
    if typeof == 'fibo':
      print "fibo_es_resp == ", es_resp
      print "count == ", count
      count = count + 1
    else:
      print "es_resp == ", es_resp
    print "\n"
  if len(esErrorData) > 0:
    with open("./entity_logs/entity_es_index_exception.txt", "w") as f:
      json.dump(esErrorData, f)
  migrateResult = {"message": "OK", "total_entities": str(len(entities)) + " entities es index regenerated.", "entity": len(data['entity']), "fibo": len(data['fibo']), "product": len(data['product']), "etf": len(data['etf']), "organization": len(data['organization']), "country": len(data['country']), "esErrorData": len(esErrorData)}
  print "migrateResult == ", migrateResult
  print "entities es index completed"

def migrateNewss():
  newss = arangoModule.get('news', {})
  print "total newss === ", len(newss)
  esErrorData = []
  count = 1
  for news in newss:
    payload = {
      "_key" : news['_key'],
      "type" : 'news',
      "attributes": news.get('attributes', []),
      "created_at": news.get('created_at', None),
      "context-type": news.get('context-type', None),
      "display": news.get('display', [])
    }
    print "payload == ", payload
    es_resp = ElasticModule.add(elasticIndex, elasticType, news['_key'], payload) 
    if "TransportError" in str(es_resp):
      file_data = {"exception" : str(es_resp), "payload": payload}
      esErrorData.append(file_data)
    print "es_resp == ", es_resp
    print "count == ", count
    count = count + 1
    print "\n"
  if len(esErrorData) > 0:
    with open("./entity_logs/news_es_index_exception.txt", "w") as f:
      json.dump(esErrorData, f)

  print "news es index completed"

def migrateFeedbacks():
  userquerys = arangoModule.get('userquery', {})
  feedbacks = arangoModule.get('feedback', {})
  print "total userquerys === ", len(userquerys)
  print "total feedbacks === ", len(feedbacks)
  esErrorDataQuery = []
  esErrorDataFeedback = []
  queryCount = 1
  fbCount = 1
  for userquery in userquerys:
    payload = {
      "_key" : userquery['_key'],
      "type" : 'userquery',
      "query": userquery.get('query', None),
      "created_at": userquery.get('created_at', None),
      "updated_at": userquery.get('updated_at', None),
      "uuid": userquery.get('uuid', [])
    }
    print "payload == ", payload
    es_resp = ElasticModule.add("feedback", "data", userquery['_key'], payload) 
    if "TransportError" in str(es_resp):
      file_data = {"exception" : str(es_resp), "payload": payload}
      esErrorDataQuery.append(file_data)
    print "es_resp == ", es_resp
    print "queryCount == ", queryCount
    queryCount = queryCount + 1
    print "\n"
  if len(esErrorDataQuery) > 0:
    with open("./entity_logs/userquery_es_index_exception.txt", "w") as f:
      json.dump(esErrorDataQuery, f)

  for feedback in feedbacks:
    feedback_payload = {
      "_key": feedback['_key'],
      "type": feedback.get('type', 'feedback'),
      "quid": feedback.get('quid', None),
      "doc_id": feedback.get('doc_id', None),
      "likes": feedback.get('likes', []),
      "dislikes": feedback.get('dislikes', []),
      "clicks": feedback.get('clicks', []),
      "source": feedback.get('source', 'ALEXA'),
      "confidence": feedback.get('confidence', None),
      "created_at": feedback.get('created_at', None),
      "updated_at": feedback.get('updated_at', None)
    }
    print "feedback_payload == ", feedback_payload
    es_resp = ElasticModule.add("feedback", "data", feedback['_key'], feedback_payload) 
    if "TransportError" in str(es_resp):
      file_data = {"exception" : str(es_resp), "payload": feedback_payload}
      esErrorDataFeedback.append(file_data)
    print "es_resp == ", es_resp
    print "fbCount == ", fbCount
    fbCount = fbCount + 1
    print "\n"
  if len(esErrorDataFeedback) > 0:
    with open("./entity_logs/feedback_es_index_exception.txt", "w") as f:
      json.dump(esErrorDataFeedback, f)

  print "feedback and userquery es index completed"

'''
def findAndReplace(  ):
  query={"query" : { "bool" : { "must" : [{ "term" : {  "type" : "answer" } },{ "match" : { "source" : "Fowradlane" } } ] } },"size" : 29 }
  data= ElasticModule.search(elasticIndex,elasticType,query)['hits']['hits']
  #{u'source': u'FowradLane', u'type': u'answer', u'question': u'5763031', u'display': u'Russia, whose economy shrank 3.7 percent in 2015 and is expected to contract again this year, is seen growing up to 2 percent in 2017 thanks to stronger oil prices and industrial production.', u'time_stamp': u'2016-10-18 19:38:19'}
  for x in data:
    _src = x['_source']
    _id  = x['_id']
    q = { "source" : "ForwardLane" , "type" : "answer" , "question" : _src['question'] , "display" : _src['display'], "time_stamp" : _src['time_stamp'] }
    ElasticModule.updateSuggestByRest(elasticIndex, elasticType, _id, json.dumps(q))
  return "1"
'''



