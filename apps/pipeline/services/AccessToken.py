import hashlib, base64

#header 			= '{"alg":"HS224","typ":"JWT"}'
#payload			= '{"from":"alexa","iat":1476364143161}'
def encode( header , payload  ):
	if header is not None and payload is not None:
		encodedHeader  = base64.b64encode(header)
		encodedPayload = base64.b64encode(payload)

		unsignedToken  = encodedHeader+"."+encodedPayload
		print unsignedToken
		signature      = hashlib.sha224(unsignedToken).hexdigest()
		print signature
		token 		   = unsignedToken +"."+ base64.b64encode(signature)
		return token
	return None

def decode( token ):
	if token is not None:
		decrypt 	     = token.split('.')
		if decrypt[2] == base64.b64encode(hashlib.sha224(decrypt[0]+"."+decrypt[1]).hexdigest()):
			return True
	return False
 