import os, json
import re
import csv
from sklearn.externals import joblib
from flask import Flask, jsonify, request
from flasgger import Swagger
from werkzeug import secure_filename
import json
from services import arangoModule, ElasticModule
from multiprocessing import Process
from arango import ArangoClient
UPLOAD_FOLDER = '/tmp/'
ALLOWED_EXTENSIONS = set(['csv'])
app = Flask(__name__)
match = type({})

db_name = "forwardlane"
collection = "entityAndQuestion"

def questionsArango(data):
  response = list()
  print "data to dump -->", data
  arango_res = arangoModule.add_arangodb(db_name, "questions", data)
  print "arango_res-->", arango_res
  payload = {
                "_key" : arango_res['_key'],
                "type" : "question",
                "display" : [data['ques']]
            }
  entity_es_res = ElasticModule.getById("entity", "data", arango_res['_key'])

  entity_res = str(entity_es_res)

  if "TransportError" in entity_res:

    es_res = ElasticModule.add("entity", "data", arango_res['_key'], payload)
    response.append(arango_res)
    response.append(es_res)
    return response
  else:
    response.append(arango_res)
    response.append(entity_res)
    return response



def arango(data):
  response = list()
  print "data to dump -->", data
  try:
    return arangoModule.add_arangodb_dump(db_name, "entities", data)
  except Exception as e:
    print ' exection arango ---> e', e
    with open("arango_dump_error_log.txt", "a") as myfile:
      data = str(data)
      myfile.write(data)
    return {'status': 'data not inserted es', 'code': 403}        

def es(type_es, key, payload):
  response = []
  entity_es_res = ElasticModule.getById("entity", type_es, key)
  entity_res = str(entity_es_res)
  print 'entity es _res -->', entity_res

  if "TransportError" in entity_res:
    try:
      return ElasticModule.add("entity", "data", key, payload)
    except Exception as e:
      with open("elastic_dump_error_log.txt", "a") as myfile:
        payload = str(payload)
        myfile.write(payload)
      return {'status': 'data not inserted es', 'code': 403}    
  else:
    response.append(entity_es_res)
    return response



















def addEntityQuestions(data):



  updated_Data_arango = arangoModule.add_arangodb(db_name, collection, data)
  payload = { "entity" : data['entity'],
            "_key" : updated_Data_arango['_key'],
            "type" : "entity",
            "suggest" : { 
              "input": data['entity'], 
              "output": data['question'],
              "payload" : data
        }
  }
  print ElasticModule.updateSuggestByRest("scrap", "data", updated_Data_arango['_key'], json.dumps(payload))
  return jsonify({ "message" : "refreshed new data", "status" : 200 })  

# def addEntity(ent, definition, src, contxt, question, force):
#   try:
#     # ent , definition , src , contxt = request.json['entity'] , request.json['definition'] , request.json['source'] , request.json['context']
#     data = { 'entity' : [ent] , 'definition' : [definition] , 'source' : [src] , 'context' : [contxt], 'question': question }
    
#     wordfile = open("static/stopwords_en.txt","r")
#     dataList = wordfile.read().split("\r")
#     wordfile.close()
#     #print dataList

#     if "\n"+ent in dataList: 
#       if not force:
#         return jsonify({ "message" : "stop words" , "status" : 400 }) 

#     arangoData = arangoModule.get(collection,{"entity" : ent })
#     print arangoData
#     if arangoData is not None:
#       if definition not in arangoData[0]['definition'] or src not in arangoData[0]['source'] or contxt not in arangoData[0]['context'] or question not in arangoData[0]['question']:
#         data = arangoData[0]
#         data['definition'].append(definition)
#         data['source'].append(src)
#         data['context'].append(contxt)
#         arangoModule.update(collection, data)
#         updated_Data = arangoModule.get(collection,{"entity" : [ent] })
        
#         #ElasticModule.update("scrap", "data", updated_Data['_key'], )
#         print updated_Data
#         payload = { "entity" : updated_Data[0]['entity'],
#                   "_key" : updated_Data[0]['_key'],
#                   "type" : "entity",
#                   "suggest" : { 
#                     "input": updated_Data[0]['entity'], 
#                     "output": updated_Data[0]['entity'],
#                     "payload" : updated_Data[0]
#               }
#         }
#         #"{\n    \"entity\" \t: [\""+updated_Data[0]['entity']+"\"],\n    \"_key\"  \t: \""+updated_Data[0]['_key']+"\",\n    \"type\"\t \t: \"entity\",\n    \"suggest\" : {\n        \"input\": +"updated_Data[0]['entity']+",\n        \"output\": \""+updated_Data[0]['entity']+"\",\n        \"weight\" : 1,\n        \"payload\" :{\n        \t\"entity\" \t: "+updated_Data[0]['entity']+",\n            \"source\" \t: "+uploaded_d+",\n            \"definition\" : [\"asdasd\",\"asdasd\"],\n            \"context\"\t: [\"verb\",\"\"]\n        }\n    }\n}"
#         #print json.dumps(payload),"here11"
#         print ElasticModule.updateSuggestByRest("scrap", "data", updated_Data[0]['_key'], json.dumps(payload))
#       return jsonify({ "message" : "refreshed new data", "status" : 200 })

#     resp = arangoModule.add(collection, data)
#     if resp is not None:
#       data['_key'] = resp['_key']
#       print data
#       payload = { "entity" : data['entity'],
#                   "_key" : resp['_key'],
#                   "type" : "entity",
#                   "suggest" : { 
#                     "input": data['entity'], 
#                     "output": data['question'],
#                     "payload" : data
#               }
#         }
#       print ElasticModule.add("scrap", "data", resp['_key'], payload)
#       return jsonify({ "status" : 200 , "info" : resp })
    
#     return jsonify({ "message" : "err adding" , "status" : 400 })
#   except Exception as e:
#     return jsonify({ "message"  : str(e) , "status" : 400 })
