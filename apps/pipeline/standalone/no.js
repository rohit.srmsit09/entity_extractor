var request = require("request");

var options = { method: 'POST',
  url: 'http://localhost:5000/addentity',
  headers: 
   { 'postman-token': 'f36ff6b7-a24d-dd5c-f456-3c0bec6e46a5',
     'cache-control': 'no-cache',
     'content-type': 'application/json' },
  body: 
   { entity: 'test india',
     definition: '',
     source: 'yahoo',
     context: 'gmaa',
     force: false },
  json: true };

var data = [{"definition": "A network address is location in a telecommunications network that may be identified by a network address (an identifier for a node or interface)", "source": "pink", "entity": "network location"}, {"definition": "A place described in terms of some abstract description or as a list of commonly understood concepts such as domestic, Eurozone etc.", "source": "pink", "entity": "notional place"}, {"definition": "A place which has no physical location.", "source": "pink", "entity": "virtual location"}, {"definition": "individual representing the Delaware Department of Corporations business entity identifier for the ABA", "source": "pink", "entity": "business entity identifier for the ABA"}, {"definition": "individual representing the headquarters address for the ABA", "source": "pink", "entity": "ABA headquarters address"}, {"definition": "individual representing the American Bankers Association (ABA) Issuer Identification Number (IIN) registry, a repository of institution characteristics for those that have assigned IINs, managed by the ABA", "source": "pink", "entity": "ABA IIN Registry"}, {"definition": "an entry in the ABA IIN registry, a repository of financial institution characteristics collected by the ABA for those institutions to which they issue IINs", "source": "pink", "entity": "ABA IIN registry entry"}, {"definition": "individual representing the registered agent address for the ABA", "source": "pink", "entity": "ABA legal address"}, {"definition": "individual representing the American Bankers Association (ABA) Routing Transit Number (RTN) registry, a repository of institution characteristics for those that have assigned RTNs, managed by the ABA\"s designated registration authority (RA)", "source": "pink", "entity": "ABA RTN Registry"}, {"definition": "an entry in the ABA RTN registry, a repository of financial institution characteristics collected by the ABA Registrar on behalf of the ABA", "source": "pink", "entity": "ABA RTN registry entry"}, {"definition": "individual representing the Accuity legal entity that is a Delaware corporation and subsidiary of Reed Business Information Limited", "source": "pink", "entity": "Accuity Inc."}, {"definition": "individual representing the Delaware Department of Corporations business entity identifier for Accuity Inc.", "source": "pink", "entity": "business entity identifier for Accuity Inc."}, {"definition": "individual representing the American Bankers Association legal entity, which is a trade association whose membership comprises financial institutions of all sizes", "source": "pink", "entity": "American Bankers Association"}, {"definition": "individual representing the American Bankers Association legal entity, which is a trade association whose membership comprises financial institutions of all sizes", "source": "pink", "entity": "ABA"}];

for(var i=0;i<data.length;i++) {
    

    options['body'] = data[i];
    
    options['body']['context'] = '';
    options['body']['force'] = false;
    console.log( options );
   request(options, function (error, response, body) {
    if (error) throw new Error(error);

    console.log(body);
  });
}



