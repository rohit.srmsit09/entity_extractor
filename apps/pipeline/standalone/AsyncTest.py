import trollius
from trollius import From

@trollius.coroutine
def greet_every_two_seconds(work, world):
    #while True:
    #    print('Hello World')
    #    yield From(trollius.sleep(2))
    return "gg"
tasks = [
    
    trollius.async(greet_every_two_seconds("work", "world"))
    ]

loop = trollius.get_event_loop()
loop.run_until_complete(trollius.wait(tasks))
print tasks[0].result()