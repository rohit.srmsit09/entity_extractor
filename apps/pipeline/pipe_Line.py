# -*- coding: utf-8 -*-
import nltk
from nltk import tokenize
import os, json , xmltodict , requests, urllib2, re, csv, math, string
from flask import Flask, jsonify, request,session
from flasgger import Swagger
from werkzeug import secure_filename
import json
from services import Syntaxnet,Opencalais, Alchemy,AccessToken, TextBlobandNltk, SpacyModule, pipeline,Standfordnlp
from multiprocessing import Process, Queue
from flasgger.utils import swag_from
from nltk.corpus import stopwords
from random import randint
import threading
from bs4 import BeautifulSoup
import datetime
import sys
import dicttoxml
# import inflect
import copy
from more_itertools import unique_everseen
from pattern.en import pluralize, singularize
from requests.auth import HTTPBasicAuth
# inflect = inflect.engine()
reload(sys)
sys.setdefaultencoding('utf-8')
stop = stopwords.words('english')
# ALLOWED_EXTENSIONS = set(['csv'])
app = Flask(__name__)
# UPLOAD_FOLDER = '/tmp/'
# match = type({})
# app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
# app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/D?RT'
Swagger(app)
retval = os.getcwd()

def splitQandDot(para):
     data = []
     newStr = ""
     for x in para:
             if x == '.' or x == '?':
                     data.append(newStr+x)
                     newStr=""
             else:
                     newStr+=x
     return data

def abort_if_todo_doesnt_exist(content):
  if content == "":
    abort(404, message="content {} can't be empty".format(content))

def allowed_file(filename):
    print filename
    return '.' in filename and \
           # filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route("/testenv")
def testEnv():
  return jsonify({ "env" : os.environ})

@app.route("/syntaxnet", methods=['POST'])
def syntaxnet():
    """
    A simple test API
    syntaxnet
    ---
    tags:
      - syntaxnet
    parameters:
      - name: string
        in: body
        type: string
        description: Sentence
    responses:
      200:
        description: A single user item
        schema:
          id: return_test
          properties:
            string:
              type: string
              description: The test
              default: 'My company will match up to 3-4% under the Fidelity Simple IRA. After I invest the max amount into that fund what do you suggest I do?'
    """

    # print request.json;
    output = list();
    content = request.data
    abort_if_todo_doesnt_exist(content)

    content_arr = splitQandDot(content)
    if content:
       print 'enter--->',content_arr

    for a in content_arr:
        # print "content-->", a
      os.system('cd ' + os.environ['syntaxnet_path'] + ' &&  echo "'+ content +'" | syntaxnet/demo.sh && cp '+os.environ['pos_tagger_path']+' '+os.environ['cp_pos_tagger_path'])
      post = joblib.load(retval + '/pos_tagger.pkl')
      # print 'post ===>', post

      for i in post:
        json_tree = json.dumps(i['d'])
        del i['d']
      output.append({'sentence': a, 'entities':post, 'syntaxnet_tree':json.loads(json_tree)})
    return jsonify({'res': output})  



@app.route("/alchemy", methods=['POST'])
def alchemy():
    """
    A simple test API
    Alchemy   -- note -- not working as the free api key service has lapsed.
    ---
    tags:
      - alchemy
    parameters:
      - name: string
        in: body
        type: string
        description: String
        
    responses:
      200:
        description: A single user item
        schema:
          id: return_test
          properties:
            string:
              type: string
              description: The test
              default: 'TURKEY INCOMING AK PARTY LEADER SAYS  AK PARTY WILL CONTINUE TO WORK IN HARMONY, CHANGE OF LEADERSHIP JUST A TOOL TO OPEN WAY FOR MORE SERVICES'
    """

    string = request.data
    # print Alchemy.process([string])
    return jsonify(Alchemy.process([string]))


@app.route("/openCalais", methods=['POST'])
def openCalais():
    """
    A simple test API
    openCalais   
    ---
    tags:
      - openCalais
    parameters:
      - name: string
        in: body
        type: string
        description: String
        
    responses:
      200:
        description: A single user item
        schema:
          id: return_test
          properties:
            string:
              type: string
              description: The test
              default: 'TURKEY INCOMING AK PARTY LEADER SAYS  AK PARTY WILL CONTINUE TO WORK IN HARMONY, CHANGE OF LEADERSHIP JUST A TOOL TO OPEN WAY FOR MORE SERVICES'
    """

    string = request.data
    # print Alchemy.process([string])
    return jsonify(Opencalais.process([string]))



@app.route("/textBlobandNltk", methods=['POST'])
@swag_from("documentation/textBlobandNltk.yml")
def extractEntities():
  try:
    texts = request.json['texts']
    if texts is not None and len(texts) > 0:
      if type([]) != type(texts):
        texts = list(texts)

      return jsonify(TextBlobandNltk.extractEntities(texts))
    else:
      return jsonify({"message": "required fields missing"}),400
  except Exception as e:
    return jsonify({"message" : str(e)}),400





@app.route("/spacy", methods=['POST'])
@swag_from("documentation/spacy.yml")
def extractEntitiesBySpacy():
  try:
    texts = request.json['texts']
    if texts is not None and len(texts) > 0:
      if type([]) != type(texts):
        texts = list(texts)

      return jsonify(SpacyModule.extractEntitiesBySpacy(texts))
    else:
      return jsonify({"message": "required fields missing"}),400
  except Exception as e:
    return jsonify({"message" : str(e)}),400

@app.route("/spacy/v2", methods=['POST'])
@swag_from("documentation/spacyV2.yml")
def extractEntitiesBySpacyV2():
  try:
    texts = request.json['texts']
    if texts is not None and len(texts) > 0:
      if type([]) != type(texts):
        texts = list(texts)

      return jsonify(SpacyModule.extractEntitiesBySpacyV2(texts))
    else:
      return jsonify({"message": "required fields missing"}),400
  except Exception as e:
    return jsonify({"message" : str(e)}),400



@app.route('/stanfordNERTagger', methods=['POST'])
@swag_from("documentation/stanford_ner.yml")
def stanfordNERTagger():
  try:
    payload = request.json
    entities = pipeline.stanfordNERTagger(payload)
    entities = json.loads(entities.get_data())['entities']
    return jsonify({'entities': entities})
  except:
    return jsonify({'entities': []})


@app.route('/stanford', methods=['POST'])
@swag_from("documentation/stanford.yml")
def stanford():
  try:
    payload = request.json
    entities = pipeline.standfordExtractor(payload)
    entities = json.loads(entities.get_data())['entities']
    return jsonify({'entities': entities})
  except:
    return jsonify({'entities': []})




@app.route('/alchemyEx', methods=['POST'])
def alchemyEx():
  try:
    payload = request.json
    entities = pipeline.alchemy(payload)
    entities = json.loads(entities.get_data())['entities']
    return jsonify({'entities': entities})
  except:
    return jsonify({'entities': []})


# Entity Extractor Pipeline Wrapper.
@app.route('/pipelineFilter', methods=['POST'])
def pipelineFilter():
  try:
    results = pipeline.pipelineFilterWrapperV1(request, stop)
    return jsonify(json.loads(results.get_data()))
  except Exception as e:
    return jsonify({"error": str(e), "results": []})

if __name__ == "__main__":
  app.run( host="0.0.0.0", debug=True, threaded=True ,use_reloader=True, port=int(os.environ.get('flask_port',5000)) )
