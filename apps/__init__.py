from flask import Flask
from .ner import ner
#add app
from  config import SECRET_KEY
#from  config import client
#from config import db,SQLALCHEMY_DATABASE_URI
from flasgger import Swagger


def create_app():
	app = Flask(__name__)
	Swagger(app)
	app.secret_key=SECRET_KEY
	app.register_blueprint(ner)
	#add blueprint
	#app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
	#app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
	#db.init_app(app)
	
	return app


#def setup_database(app):
	#with app.app_context():
		#db.create_all()


app=create_app()
#setup_database(app)
