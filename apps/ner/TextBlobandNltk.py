# -*- coding: utf-8 -*-
import sys, nltk, re
from textblob import TextBlob
from nltk.corpus import stopwords
# relad(sys)
# sys.setdefaultencoding('utf-8')
stop = stopwords.words('english')


def extractEntities(texts):
    try:
        # path = './static/stopwords_en.txt'
        # stop = []
        # with open(path, 'r') as stopwords:
        #   stop = stopwords.read().split('\n')

        # stop = [x.replace('\r', '') for x in stop]
        # import ipdb; ipdb.set_trace()
        stop.append('%')
        extractedEntities = []
        if type([]) != type(texts):
            texts = list(texts)
        for t in texts:
            # text = t.encode('ascii','ignore')
            text = t.replace(',', '')
            text = text.replace('.', ' ')
            text = text.replace('-', ' ')
            text = text.replace('+', '')
            text = text.replace("'", ' ')
            text = re.sub(r'\d+', '', text)
            # print "text == ", text
            blob = TextBlob(text)
            txtBlbNouns = blob.noun_phrases
            txtBlbResult = [x.lower() for x in txtBlbNouns]
            # print "txtBlbResult == ", txtBlbResult
            # print "\n"
            tokens = nltk.word_tokenize(text)
            tagged = nltk.pos_tag(tokens)
            nltkResult = [x[0].lower() for x in tagged if x[1] == 'NN' or x[1] == 'NNP']
            # print "nltkResult == ", nltkResult
            extractedEntities = list(set(extractedEntities+txtBlbResult+nltkResult))
        return {'entities': [x for x in extractedEntities if x not in stop and len(x.split(' ')) <= 5]}
    except Exception as e:
        print ("error ===> ", e)
        return {"message": str(e), "status": 400, "entities": []}


def extractEntitiesByNLTK(texts):
    try:
        # path = './static/stopwords_en.txt'
        # stop = []
        # with open(path, 'r') as stopwords:
        #   stop = stopwords.read().split('\n')

        # stop = [x.replace('\r', '') for x in stop]
        stop.append('%')
        extractedEntities = []
        if type([]) != type(texts):
            texts = list(texts)
        for t in texts:
            text = t.decode('unicode_escape').encode('ascii','ignore')
            text = text.replace(',', '')
            text = text.replace('.', ' ')
            text = text.replace('-', ' ')
            text = text.replace('+', '')
            text = text.replace("'", ' ')
            text = re.sub(r'\d+', '', text)
            # print "text == ", text
            tokens = nltk.word_tokenize(text)
            tagged = nltk.pos_tag(tokens)
            nltkResult = [x[0].lower() for x in tagged if x[1] == 'NN' or x[1] == 'NNP']
            # print "nltkResult == ", nltkResult
            extractedEntities = list(set(extractedEntities+nltkResult))
        return {'entities': [x for x in extractedEntities if x not in stop and len(x.split(' ')) <= 5]}
    except Exception as e:
        print ("error ===> ", e)
        return {"message": str(e), "status": 400, "entities": []}
