import nltk
import json
from nltk import Tree
def ie_preprocess(document):
	sentences = nltk.sent_tokenize(document)
	sentences = [nltk.word_tokenize(sent) for sent in sentences]
	sentences = [nltk.pos_tag(sent) for sent in sentences]
	return sentences

def chunk(sentences):
	data = ie_preprocess(sentences)	
	grammar = "NP: {<.*>+}"
	cp = nltk.RegexpParser(grammar)
	result = cp.parse(data[0])
	return result

# print chunking('Will I be able to file for Social Security. spousal benefits at age 66 and then switch to my own at age 70?')
