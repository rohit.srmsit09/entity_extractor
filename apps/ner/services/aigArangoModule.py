from arango import ArangoClient
import os
# Initialize the ArangoDB client as root
client = ArangoClient(protocol='http',host=os.environ.get('arango_db','104.196.154.5'),port=8529,username=os.environ.get('forwardlane_db_username','root'), password=os.environ.get('forwardlane_db_pass','forward123'))

db_name = os.environ.get('aig_db', 'aig')  # os.environ['forwardlane_db']
# db_uname = os.environ.get('aig_db_username','root')
# db_passwd = os.environ.get('aig_db_pass','forward123')
db = client.db(db_name)

graph = db.graph("words_relation")
words = graph.vertex_collection("words")
words_attributes = graph.vertex_collection("words_attributes")

words_relation = graph.edge_collection("words_relation")


wordfile = open("static/Punctuation.txt","r")
dataList = wordfile.read().split("\r")
wordfile.close()
# print dataList

match = type({})

try:
  db.properties()
except Exception as e:
  print 'e -->', e
  # db = client.create_database(db_name, username=db_uname, password=db_passwd)

else:
  print "Database Connection Successfull!"


# try:
#     db.create_collection('testcollection')
# except Exception as e:
#   print "collection exists ",e
# db.create_collection('testcollection')

# Create a database, again as root (the user is inherited if not specified)

# db = client.create_database('my_database', username=None, password=None)

# db = client.db('my_database')
# testcollection = db.collection('testcollection')

# Retrieve the properties of the new database

# Create another database, this time with a predefined set of users


def add(collectionName, data ):
    if data is None:
        return None

    testcollection = db.collection(collectionName)
    result = testcollection.insert(data)
    # print result
    return result


def get(collectionName, query):
    # print "get arngod query ->", query
    testcollection = db.collection(collectionName)
    record = testcollection.find(query)
    dataCollection = []
    for student in record:
        dataCollection.append(student)
    if len(dataCollection)<=0:
      # print "sent none"
      return None
    # print len(dataCollection)
    # print '--->',dataCollection
    return dataCollection

def getMany(collectionName, query):
    testcollection = db.collection(collectionName)

    totalData = []
    for x in query:
      print x
      record = testcollection.find({ "entity" : [x] }) 
      # print record
      if record is not None:
        for r in record:
          # print "entered"
          totalData.append( r )
    '''  
    record = testcollection.find(query)
    dataCollection = []
    for student in record:
        dataCollection.append(student)
    if len(dataCollection)<=0:
      return None
    #print len(dataCollection)
    '''
    if len(totalData)<=0:
      return None
    # print totalData
    return totalData


def add_arangodb( db_name, coll_name , data ):
  if (coll_name or db_name or data) is None:
    return None
  db = client.db(db_name)

  try:
    db.collection(coll_name) 
  except:
    db.create_collection(coll_name)
  else: 
    coll = db.collection(coll_name) 
    resp = get(coll_name, data)
    if resp is None:
      result = coll.insert(data)
      return result    
    else:
       return resp[0]

def get_test(collectionName, query):
    # print "get arngod query ->", query
    testcollection = db.collection(collectionName)
    record = testcollection.find(query)
    dataCollection = []
    # print 'record found -->', record
    for student in record:
        dataCollection = student
    if len(dataCollection)<=0:
      return None
    return dataCollection


def add_arangodb_dump( db_name, coll_name , data ):
  if (coll_name or db_name or data) is None:
    return None
  db = client.db(db_name)

  try:
    db.collection(coll_name) 
  except:
    db.create_collection(coll_name)
  else: 
    coll = db.collection(coll_name) 
    resp = get_test(coll_name, data)
    # print 'resp of arango get-->', resp

    if resp is None:
      result = coll.insert(data)
      return result    
    else:
      return resp


def create_graph( graph_name ):
  try:
    db.graph(graph_name).properties()
  except:
    db.create_graph(graph_name)
  else:
    return True


def update(collectionName, data):
  testcollection = db.collection(collectionName)
  return testcollection.update(data)


def create_entity_question_graph(obj, parent=None):
  # finaldata = []
  for x in obj:
    print 'x->', x

    if type(obj) == match:
      word_attr = x.split(" ")

      if "\n"+word_attr[0] not in dataList[0]: 
        # word_attr = word_attr[1:]
        word = get('words', {"name":word_attr[0]})
        key = word_attr[0]
        if word is None:
          valueKeys = add('words', {'_key':key, "name": word_attr[0]})
          # print "res", word_att
        else:
            valueKeys = word[0]       
    # print "valueKeys-->", valueKeys
      else:
        word_attr = word_attr[1:]
        word = get('words', {"name":word_attr[0]})
        key = word_attr[0]
        if word is None:
          valueKeys = add('words', {'_key':key, "name": word_attr[0]})
          # print "res", word_att
        else:
            valueKeys = word[0]       
    print "valueKeys-->", valueKeys        

    # print "word_attr", word_attr
    wordlength = len(word_attr)
    i = 1
    if wordlength:
      while i < wordlength:
        # print 'word -->', i
        res = get('words', {"name": word_attr[i]})
        # print 'response -->', res

        if res is None:
          word_att =add('words', {'_key':word_attr[i], "name": word_attr[i]})
          # print "res", word_att
        else:
            word_att = res[0] 
        print "word to insert int w_attr __>", word_att 
        words_relation.insert({ "_from": word_att['_id'], "_to": valueKeys['_id'] })     
        i += 1 
    if parent is not None:
      words_relation.insert({ "_from": "words/"+parent, "_to": "words/"+valueKeys['_key'] })
    create_entity_question_graph(obj[x], valueKeys['_key'])
  return "Graph has been created"  


def queryArrayValue(collectionName, text, field):
  cursor = db.aql.execute("FOR a IN "+collectionName+" FILTER '"+text+"' IN a['"+field+"'] RETURN a")
  data = []
  if cursor is not None:
    for r in cursor:
      data.append(r)

  if len(data)<=0:
    return None

  return data


def addGraph( listArray ):
  try:
    print listArray
    testGraph = db.graph('relation')

    # Create a couple of vertex collections
    qst= testGraph.vertex_collection('questions')
    tc =  testGraph.vertex_collection('testcollection')

    # Create a new edge definition (and a new edge collection)
    test_relation = testGraph.edge_collection('edge')
    for x in listArray:
      print test_relation.insert(x), "added node"

    return { "status" : 200 }
  except Exception as e:
    return { "status" : 400 , "message" : str(e) }

def getById(collectionName, id):
  try:
    testcollection = db.collection(collectionName)
    return testcollection.get(id)
  except Exception as e:
    return { "status": 400, "message" : str(e) }


def getMultipleIds(collectionName, ids):
  try:
    dataCollection = db.collection(collectionName)
    result = dataCollection.get_many(ids)
    if len(result)>0:
      return result

    return None
  except Exception as e:
    return { "status": 400, "message": str(e) }
  # return listArray


def arango_logs(curr_date):
  query='FOR user IN entities FILTER user.created_at == @name RETURN user '
  cur_entity=db.aql.execute(query,bind_vars={'name':curr_date}) #for entity
  en_len=len(list(cur_entity))
  a= "entity inserted on "+curr_date+" "

  query='FOR user IN entities_relation FILTER user.created_at == @name RETURN user '
  cur_entity_relation=db.aql.execute(query,bind_vars={'name':curr_date})
  en_rel_len=len(list(cur_entity_relation))
  b="entity_relataion inserted on "+curr_date+" "

  query='FOR user IN news FILTER user.created_at == @name RETURN user '
  cur_news=db.aql.execute(query,bind_vars={'name':curr_date})
  news_len=len(list(cur_news))
  c="news inserted on "+curr_date+" "

  cu=curr_date+"%"
  query='''FOR user IN answers FILTER user.time_stamp LIKE @name RETURN user '''
  cur_ans=db.aql.execute(query,bind_vars={'name':cu})
  ans_len=len(list(cur_ans))
  d="answer inserted on "+curr_date+" "

  query='FOR user IN questions FILTER user.time_stamp LIKE @name RETURN user'
  cur_ques=db.aql.execute(query,bind_vars={'name':cu})
  ques_len=len(list(cur_ques))
  e="question inserted on "+curr_date+" "

  return {a:en_len,b:en_rel_len,c:news_len,d:ans_len,e:ques_len}

'''
db = client.create_database(
    name='another_database',
    users=[
        {'username': 'jane', 'password': 'foo', 'active': True},
        {'username': 'john', 'password': 'bar', 'active': True},
        {'username': 'jake', 'password': 'baz', 'active': True},
    ],
    username='jake',  # The new database object uses jake's credentials
    password='baz'
)

# To switch to a different user, simply create a new database object with
# the credentials of the desired user (which in this case would be jane's)


# Delete an existing database as root
client.delete_database('another_database')
'''
