import nltk
import spacy
# import inflection
import requests
from textblob import TextBlob
from flask import Flask, jsonify
import json
import re
import os 
import gc
# import string
from nltk.corpus import stopwords
from simplejson import loads
# from apps.ner.services import Alchemy
# from apps.ner.standalone import jsonrpc
from nltk.tag import StanfordNERTagger
from multiprocessing import Pool,Queue
import threading
stop = stopwords.words('english')

app = Flask(__name__)

# server = jsonrpc.ServerProxy(jsonrpc.JsonRpc20(), jsonrpc.TransportTcpIp(addr=("127.0.0.1", 8080), timeout=40.00))
# to run corenlp run --> python corenlp.py -H 0.0.0.0 -p 8080
# dir_path = os.path.dirname(os.path.realpath(__file__))

calais_url = "https://api.thomsonreuters.com/permid/calais"
headers = {'outputFormat': "application/json", 'x-ag-access-token': 'bVhFc6RBgyAd5ME9lkzB1dEbv0DPrycG'}

cachedStopWords = stopwords.words("english")


def nltkExtractor(query, callback=None):
    # print "threading nltk extractor.",query
    nouns_list = []
    try:
        for sentence in query['texts']:
            sentence = sentence.replace(".", " ")

            tokens = nltk.word_tokenize(sentence)
            tagged = nltk.pos_tag(tokens)
            for tag in tagged:
                if tag[1] == 'NN' or tag[1] == 'NNP':
                    if tag[0].lower() not in nouns_list and tag[0].lower() not in cachedStopWords:
                        if re.match("^[a-zA-Z0-9_]*$", tag[0].lower()[0]):
                            word = tag[0].lower()
                            if word[-1] == '/':
                                word = word[:-1]
                            nouns_list.append(word)
        if callback is None:
            return jsonify({"entities": nouns_list})
        
        callback.put( { "entities" :  nouns_list, "function": "nltkExtractor"  } )

    except:
        if callback is None:
            return jsonify({"entities": nouns_list})
        
        callback.put( { "entities" :  nouns_list, "function": "nltkExtractor" } )        


def textBlobExtractor(query, callback=None):
    nouns_list = []
    try:
        for sentence in query['texts']:
            sentence = sentence.replace(".", " ")

            text = TextBlob(sentence)
            for noun_phrase in text.noun_phrases:
                # noun_phrase = inflection.singularize(noun_phrase)
                if not re.match("^[a-zA-Z0-9_]*$", noun_phrase.lower()[-1]):  # Removing special chars from last
                    noun_phrase = noun_phrase[:-1]
                if re.match("^[a-zA-Z0-9_]*$", noun_phrase.lower()[0]):
                    if noun_phrase.lower() not in nouns_list and noun_phrase.lower() not in cachedStopWords:
                        noun_phrase = noun_phrase.lower()
                        noun_phrase_list = noun_phrase.split()
                        phrase_list = []
                        for phrase in noun_phrase_list:
                            if phrase[-2] == ' ':
                                phrase = phrase[:(len(phrase) - 2)] + phrase[(len(phrase) - 1):]  # fixing words like 'king s'
                            if phrase[-1] == '/':
                                phrase = phrase[:-1]

                            if phrase[0] == '$' and re.match("^[0-9]*$", phrase[1]):  # removing entities like '$400'
                                if ' ' in phrase:
                                    i = phrase.index(' ')
                                    if not re.match("^[a-zA-Z]*$", phrase[i + 1]):  # allowing entities like '$400 rupees'
                                        continue
                                else:
                                    continue
                            if not re.match("^[a-zA-Z-Z0-9_$]*$", phrase[0]):
                                continue
                            phrase_list.append(phrase)
                        noun_phrase = " ".join(phrase_list)
                        nouns_list.append(noun_phrase)

        if callback is None:
            return jsonify({"entities": nouns_list})
        
        callback.put( { "entities" :  nouns_list, "function": "textBlobExtractor"  } )

    except:
        if callback is None:
            return jsonify({"entities": nouns_list})
        
        callback.put( { "entities" :  nouns_list, "function": "textBlobExtractor"  } )


# def standfordExtractor(query,callback=None):
#     try:
#         entities_list = []
#         for text in query['texts']:
#             text = text.replace("\\", "")
#             text = text.replace(".", ". ")
#             text_list = text.split('. ')
#             for sentence in text_list:
#                 words = sentence.split()
#                 for word in words:
#                     if word in cachedStopWords:
#                         words.remove(word)
#                 sentence = ' '.join(words)

#             for text in text_list:
#                 if len(text) < 1000:
#                     sentences = loads(server.parse(text))['sentences']
#                     for sentence in sentences:
#                         for word in sentence['words']:
#                             if word[1]['PartOfSpeech'] == 'NNP' or word[1]['PartOfSpeech'] == 'NN':
#                                 if word[0].lower() not in entities_list and word[0].lower() not in cachedStopWords:
#                                     entities_list.append(word[0].lower())
#             results = {"entities": entities_list}
#             return jsonify(results)
#         if callback is None:
#             return jsonify({"entities": entities_list})
        
#         callback.put( { "entities" :  entities_list, "function": "standfordExtractor"  } )
#     except:

#         if callback is None:
#             return jsonify({"entities": entities_list})

#         callback.put( { "entities" : entities_list, "function": "standfordExtractor" } )


def opencalaisExtractor(query,callback=None):
    entities = []
    try:
        for text in query['texts']:
            payload = text
            response = requests.request("POST", calais_url, data=json.dumps(payload), headers=headers)
            res = response.json()
            res = json.dumps(res)
            res_load = json.loads(res)
            calis_all_keys = res_load.keys()

            entites_keys = calis_all_keys
            if len(entites_keys):
                for key in entites_keys:
                    try:
                        if res_load[key]['_typeGroup'] == 'entities':
                            if res_load[key]['name'].lower() not in entities and res_load[key]['name'].lower() not in cachedStopWords:
                                entities.append(res_load[key]['name'].lower())
                    except:
                        pass
        if callback is None:
            gc.collect()
            return jsonify({"entities": entities})
        gc.collect()
        callback.put( { "entities" : entities, "function": "opencalaisExtractor"   } )
    except:
        if callback is None:
            gc.collect()
            return jsonify({"entities": entities})
        gc.collect()
        callback.put( { "entities" : entities, "function": "opencalaisExtractor"  } )        


def spacyExtractor(query,callback=None):
    entities_list = []
    try:
        nlp = spacy.load('en')
        for text in query['texts']:
            nlp_obj = nlp(text)
            for token in nlp_obj:
                if token.pos_ == 'PROPN' or token.pos_ == 'NOUN':
                    token = str(token)
                    if token not in entities_list:
                        # if not re.match("^[a-zA-Z0-9_]*$", token.lower()[-1]):  # Removing special chars from last
                        #     token = token[:-1]

                        # if not re.match("^[a-zA-Z0-9_#]*$", token.lower()[0]):  # removing entities like '$400' or '+9'
                        #     if ' ' in token:
                        #         i = token.index(' ')
                        #         if not re.match("^[a-zA-Z]*$", token[i + 1]):  # allowing entities like '$400 rupees'
                        #             continue
                        #     else:
                        #         continue
                        # if token.lower() not in cachedStopWords:
                        #     entities_list.append((str(token)).lower())
                        entities_list.append((str(token)).lower())
        if callback is None:
            return jsonify({"entities": entities_list})
        
        callback.put( { "entities" :  entities_list, "function": "spacyExtractor"    } )
    except:
        if callback is None:
            return jsonify({"entities": entities_list})

        callback.put( { "entities" : entities_list, "function": "spacyExtractor"   } )


# def alchemy(query,callback=None):
#     entity_list = []
#     try:
#         for text in query['texts']:
#             entities = Alchemy.process([text])
#             for entity in entities['entities']:
#                 # if entity not in entity_list and len(entity) > 1:
#                 #     if not re.match("^[a-zA-Z0-9_]*$", entity.lower()[-1]):  # Removing special chars from last
#                 #         entity = entity[:-1]

#                 #     if not re.match("^[a-zA-Z0-9_#]*$", entity.lower()[0]):  # removing entities like '$400' or '+9'
#                 #         if ' ' in entity:
#                 #             i = entity.index(' ')
#                 #             if not re.match("^[a-zA-Z]*$", entity[i + 1]):  # allowing entities like '$400 rupees'
#                 #                 continue
#                 #         else:
#                 #             continue
#                 #     if entity.lower() not in cachedStopWords:
#                 #         entity_list.append((str(entity)).lower())
#                 entity_list.append(entity.lower())
#         if callback is None:
#             return jsonify({"entities": entity_list})
        
#         callback.put( { "entities" :  entity_list, "function": "alchemy"    } )
#     except:
#         if callback is None:
#             return jsonify({"entities": entity_list})

#         callback.put( { "entities" : entity_list, "function": "alchemy"   } )


def stanfordNERTagger(query,callback=None):
    entities = []
    try:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        # print dir_path
        dir_path = dir_path.rsplit("/",1)[0]
        print (dir_path)
        path_1 = dir_path + '/stanford-ner-2015-04-20/classifiers/english.all.3class.distsim.crf.ser.gz'
        path_2 = dir_path + '/stanford-ner-2015-04-20/stanford-ner.jar'
        print(path_1)
        print(path_2)
        # import ipdb; ipdb.set_trace()
        for text in query['texts']:
            st = StanfordNERTagger(path_1, path_2)
            tokens = st.tag(text.split())
            for token in tokens:
                # print "token[1] == ", token[1]
                if token[1] == 'ORGANIZATION' or token[1] == 'PERSON' or token[1] == 'LOCATION':
                    # print "token[0] == ", token[0]
                    entities.append(token[0].lower())
                    # entities.append(token)
        if callback is None:
            return {"entities": entities}
        
        callback.put( { "entities" :  entities, "function": "stanfordNERTagger"    } )

    except Exception as e:
        print ("error in stanfordNERTagger === ", str(e))
        if callback is None:
            return {"entities": entities}

        callback.put( { "entities" : entities, "function": "stanfordNERTagger"   } )


def pipelineFilterWrapper(request, stop):
  # pipe_list = [nltkExtractor, textBlobExtractor, standfordExtractor, opencalaisExtractor, spacyExtractor, stanfordNERTagger, alchemy]
  '''{
    "texts": ["Facebook added about $335.34B billoion"]
    }'''
  pipe_list = [nltkExtractor, textBlobExtractor]
  dispatcher = {'pipeline': pipe_list}
  results = []
  try:
    payload = request.json
    for func in dispatcher['pipeline']:
      entities = func(payload)
      entities = json.loads(entities.get_data())['entities']

      for entity in entities:
        if entity not in results and len(entity) > 1:
          if not re.match("^[a-zA-Z0-9_]*$", entity.lower()[-1]):  # Removing special chars from last
            entity = entity[:-1]

          if not re.match("^[a-zA-Z0-9_#]*$", entity.lower()[0]):  # removing entities like '$400' or '+9'
            if ' ' in entity:
              i = entity.index(' ')
              if not re.match("^[a-zA-Z]*$", entity[i + 1]):  # allowing entities like '$400 rupees'
                continue
            else:
              continue
          if entity.lower() not in stop and entity not in stop:
            results.append((str(entity)).lower())

    results = set(results)
    results = list(results)

    return jsonify({"results": results})
  except:
    return jsonify({"results": results})


def nltkExtractor_raw(query, callback=None):
    nouns_list = []
    return_list = []
    try:
        for sentence in query['texts']:
            sentence = sentence.replace(".", " ")

            tokens = nltk.word_tokenize(sentence)
            tagged = nltk.pos_tag(tokens)
            return_list.append(tagged[0][0])

            tagged.pop(0)
            if len(tagged) > 0:
                count = 0
                for tag in tagged:
                    if tag[1] == 'NN' or tag[1] == 'NNP':

                        if tag[0].lower() not in cachedStopWords:
                            if re.match("^[a-zA-Z0-9_]*$", tag[0].lower()[0]):
                                word = tag[0]
                                if word[-1] == '/':
                                    word = word[:-1]
                                return_list.append(word)
                                count += 1
                                continue
                    else:
                        for i, v in enumerate(tagged):
                          if i >=count:
                            nouns_list.append(v[0])
                        break

        return {"nouns_list": nouns_list, "return_list": return_list}

    except:
        return {"nouns_list": [], "return_list": []}


def pipelineFilterWrapperV1(request):
  """this is a wrapper fuction for entity extraction using stanfordNERTagger, opencalais , spacy, textBlob, nltk, stanfordnlp services"""
  try:
    # task_list = [nltkExtractor,textBlobExtractor, opencalaisExtractor, spacyExtractor, stanfordNERTagger,standfordExtractor]
    task_list = [nltkExtractor,textBlobExtractor,spacyExtractor,stanfordNERTagger]
    max_task  = len(task_list)
    callback = [ Queue() ] * max_task
    pipe_list = []
    appendRef = pipe_list.append

    for x in range( max_task ):
      appendRef(   threading.Thread(target=task_list[x] , args=( request.json , callback[x] ) ) )
    
    for x in pipe_list:
      print (x)
      x.start()

    # for x in callback:  # Printing the results
    #   print x.get()

    for x in pipe_list:  # Printing 
      x.join()

    results = []
    for x in callback:
      entities = x.get()['entities']

      for entity in entities:
        if entity not in results and len(entity) > 1:
          if not re.match("^[a-zA-Z0-9_]*$", entity.lower()[-1]):  # Removing special chars from last
            entity = entity[:-1]

          if not re.match("^[a-zA-Z0-9_#]*$", entity.lower()[0]):  # removing entities like '$400' or '+9'
            if ' ' in entity:
              i = entity.index(' ')
              if not re.match("^[a-zA-Z]*$", entity[i + 1]):  # allowing entities like '$400 rupees'
                continue
            else:
              continue
          if entity.lower() not in stop and entity not in stop:
            results.append((str(entity)).lower())

    results = set(results)
    results = list(results)

    return {"results": results  }
  except Exception as e:
    return {"results": str(e) }


