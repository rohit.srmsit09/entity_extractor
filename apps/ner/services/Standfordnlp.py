from standalone import jsonprc
from  more_itertools import unique_everseen
from services import Chunking
from simplejson import loads

import json


server = jsonprc.ServerProxy(jsonprc.JsonRpc20(),
                             jsonprc.TransportTcpIp(addr=("127.0.0.1", 8080), timeout=5000.00))

# text = "I be able to file for Social Security. spousal benefits at age 66 and then switch to my own at age 70?"

def process(text_arr):
	stndfrd_entities = []
	chunk_sentences = []
	for text in text_arr:
		# print 'text -->', text
		# text = text.replace("#", " ")
		try:
			result = loads(server.parse(text))
			# print "Result", result
			chunk_sentences.append({'title': text})
			for w in result['sentences']:
				for i in 	w['words']:
					if i[1]['PartOfSpeech'].startswith('NNP'):
						stndfrd_entities.append(i[1]['Lemma'])
			return {'entities': list(unique_everseen(stndfrd_entities)), "chunck":result['sentences'][0]['parsetree']}
		except Exception as e:
			print 'e --->', e
			return 403				

	# return {'entities': list(unique_everseen(stndfrd_entities))}
	# print 'stndfrd_entities -->', stndfrd_entities
# except Exception as e:
# 	print 'e -->', e		
