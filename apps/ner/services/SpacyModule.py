# -*- coding: utf-8 -*-
import  spacy
import sys, copy
import re
from nltk.corpus import stopwords
stop = stopwords.words('english')
parser = spacy.load('en')
def extractEntitiesBySpacy(texts):
    try:
        extractedEntities = []
        if type([]) != type(texts):
            texts = list(texts)
        for t in texts:
            text = t#.decode('unicode_escape').encode('ascii','ignore')
            text = re.sub(r'\d+', '', text)
            # text = unicode(text, encoding="utf-8")
            parsedText = parser(text)
            entities = list(parsedText.ents)
            entities = [str(x).lower() for x in entities]
            extractedEntities = list(set(extractedEntities+entities))
        return {'entities': extractedEntities}
    except Exception as e:
        return {"message": str(e), "status": 400}


def extractEntitiesBySpacyV2(texts):
    try:
        extractedEntities = []
        wordTreeOutput = []
        if type([]) != type(texts):
            texts = list(texts)
        for t in texts:
            text = t#.decode('unicode_escape').encode('ascii','ignore')
            text = re.sub(r'\d+', '', text)
            # text = unicode(text, encoding="utf-8")
            parsedText = parser(text)
            entities = list(parsedText.ents)
            output = []
            wordTree = []
            for x in entities:
                px = parser(str(x))
                for token in px:
                    t = {
                        "word": str(token.orth_).lower(),
                        "pos": token.pos_
                    }
                    wordTree.append(t)
                d = {
                    "entity": str(x).lower(),
                    "label": x.label_
                }
                output.append(d)
            extractedEntities = list(extractedEntities+output)
            wordTreeOutput = list(wordTreeOutput+wordTree)
            extractedEntities = [dict(t) for t in set([tuple(d.items()) for d in extractedEntities])]
            wordTreeOutput = [dict(t) for t in set([tuple(d.items()) for d in wordTreeOutput])]
        return {'entities': extractedEntities, "wordTree": wordTreeOutput}
    except Exception as e:
        return {"message": str(e), "status": 400}


def extract_word_tree_v2(replace, current_path):
    try:
        with open(current_path + "/Stanford_OpenIE_Python_master/samples.txt", "r") as myfile:
            texts = myfile.read()
        texts = [texts]
        for t in texts:
            text = t.decode('unicode_escape').encode('ascii','ignore')
            text = re.sub(r'\d+', '', text)
            text = unicode(text, encoding="utf-8")
            parsedText = parser(text)
            return_list = []
            for i in parsedText:
              if replace in (i.text).lower():
                dep = i.dep_
                break

            for i in parsedText:
              if dep in i.dep_:
                if (str(i)).lower() != replace:
                  if (str(i)).lower() not in stop:
                    return_list.append(str(i))
                    break
        return {'results': return_list}
    except Exception as e:
        return {"error": str(e), "status": 400}

