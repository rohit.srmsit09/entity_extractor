from . import ner
from . models import *
from . views import firstapp, extractEntities,extractEntitiesBySpacy,extractEntitiesBySpacyV2,stanfordNERTagger,pipelineFilter
# from flask import request,jsonify


ner.add_url_rule('/ner/firstapp',methods=['GET'], view_func=firstapp)
ner.add_url_rule("/ner/textBlobandNltk", methods=['POST'],view_func=extractEntities)
# ner.add_url_rule("/ner/openCalais", methods=['POST'],view_func=openCalais)
ner.add_url_rule("/ner/spacy", methods=['POST'],view_func=extractEntitiesBySpacy)
ner.add_url_rule("/ner/spacy/v2", methods=['POST'],view_func=extractEntitiesBySpacyV2)
ner.add_url_rule("/ner/stanfordNERTagger", methods=['POST'],view_func=stanfordNERTagger)
# ner.add_url_rule("/ner/stanford", methods=['POST'],view_func=stanford)
# ner.add_url_rule("/ner/alchemyEx", methods=['POST'],view_func=alchemyEx)
ner.add_url_rule("/ner/pipelineFilter", methods=['POST'],view_func=pipelineFilter)
