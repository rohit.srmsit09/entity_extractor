from . models import *
from flask import request,jsonify
from flasgger.utils import swag_from
from . services import TextBlobandNltk,SpacyModule,pipeline



def firstapp():
	return jsonify({'msg':'first app'})


@swag_from("/apps/documentation/textBlobandNltk.yml")
def extractEntities():
	try:
		import os
		print(os.getcwd())
		texts = request.json['texts']
		if texts is not None and len(texts) > 0:
			if type([]) != type(texts):
				texts = list(texts)

			return jsonify(TextBlobandNltk.extractEntities(texts))
		else:
			return jsonify({"message": "required fields missing"}),400
	except Exception as e:
		return jsonify({"message" : str(e)}),400


@swag_from("/apps/documentation/spacy.yml")
def extractEntitiesBySpacy():
	try:
		texts = request.json['texts']
		if texts is not None and len(texts) > 0:
			if type([]) != type(texts):
				texts = list(texts)

			return jsonify(SpacyModule.extractEntitiesBySpacy(texts))
		else:
			return jsonify({"message": "required fields missing"}),400
	except Exception as e:
		return jsonify({"message" : str(e)}),400


@swag_from("/apps/documentation/spacyV2.yml")
def extractEntitiesBySpacyV2():
	try:
		texts = request.json['texts']
		if texts is not None and len(texts) > 0:
			if type([]) != type(texts):
				texts = list(texts)

			return jsonify(SpacyModule.extractEntitiesBySpacyV2(texts))
		else:
			return jsonify({"message": "required fields missing"}),400
	except Exception as e:
		return jsonify({"message" : str(e)}),400


@swag_from("/apps/documentation/stanford_ner.yml")
def stanfordNERTagger():
	try:
		payload = request.json
		entities = pipeline.stanfordNERTagger(payload)
		return jsonify(entities)
	except:
		return jsonify({'entities': []})


# @swag_from("documentation/stanford.yml")
# def stanford():
# 	try:
# 		payload = request.json
# 		entities = pipeline.standfordExtractor(payload)
# 		entities = json.loads(entities.get_data())['entities']
# 		return jsonify({'entities': entities})
# 	except:
# 		return jsonify({'entities': []})


# def alchemyEx():
# 	try:
# 		payload = request.json
# 		entities = pipeline.alchemy(payload)
# 		entities = json.loads(entities.get_data())['entities']
# 		return jsonify({'entities': entities})
# 	except:
# 		return jsonify({'entities': []})


# Entity Extractor Pipeline Wrapper.
@swag_from("/apps/documentation/pipeline.yml")
def pipelineFilter():
	try:
		results = pipeline.pipelineFilterWrapperV1(request)
		return jsonify(results)
	except Exception as e:
		return jsonify({"error": str(e), "results": []})
